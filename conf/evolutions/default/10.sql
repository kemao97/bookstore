# --- !Ups
CREATE TABLE `payment` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `payment_type` VARCHAR(100) NOT NULL
);

# --- !Downs
DROP TABLE IF EXISTS `payment`;