# --- !Ups
CREATE TABLE cart (
  cart_id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  customer_id INT UNSIGNED NOT NULL,
  book_id INT UNSIGNED NOT NULL,
  book_number INT NOT NULL,
  CONSTRAINT fk_cart_customer FOREIGN KEY (customer_id) REFERENCES customer(customer_id),
  CONSTRAINT fk_cart_book FOREIGN KEY (book_id) REFERENCES book(book_id)
);

# --- !Downs
ALTER TABLE cart DROP FOREIGN KEY fk_cart_customer;
ALTER TABLE cart DROP FOREIGN KEY fk_cart_book;
DROP TABLE IF EXISTS cart;
