# --- !Ups
CREATE TABLE book (
  book_id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  book_name VARCHAR(100) NOT NULL,
  book_type VARCHAR(100) NOT NULL,
  book_cost INT NOT NULL,
  book_author VARCHAR(500) NOT NULL,
  book_produce VARCHAR(500) NOT NULL,
  book_discount INT(3) NOT NULL,
  book_note VARCHAR(500),
  book_image VARCHAR(500) NOT NULL,
  is_lock TINYINT(1) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_by VARCHAR(40),
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  updated_by VARCHAR(40)
);

# --- !Downs
DROP TABLE IF EXISTS book;
