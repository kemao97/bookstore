# --- !Ups
INSERT INTO account(account_name, account_password, account_position, is_lock)
  VALUES ('admin', 'password', 'admin', false);
INSERT INTO staff(staff_id, staff_name, staff_acc_name, staff_age, staff_address, staff_phone,
                      staff_salary, staff_note, created_by, updated_by)
  VALUES (null, 'admin', 'admin', 21, '', '', 0, '', 'admin', 'admin');

# --- !Downs
DELETE FROM staff WHERE staff_acc_name = 'admin';
DELETE FROM account WHERE account_name = 'admin';