# --- !Ups
CREATE TABLE `order` (
  order_id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  customer_id INT UNSIGNED NOT NULL,
  order_cost INT NOT NULL,
  order_address VARCHAR(500),
  order_status VARCHAR(100) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_by VARCHAR(40),
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  updated_by VARCHAR(40),
  CONSTRAINT fk_order_customer FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);

# --- !Downs
ALTER TABLE `order` DROP FOREIGN KEY fk_order_customer;
DROP TABLE IF EXISTS `order`;
