# --- !Ups
ALTER TABLE `order` ADD COLUMN `payment_id` INT AFTER `customer_id`;

# --- !Downs
ALTER TABLE `order` DROP COLUMN `payment_id`;