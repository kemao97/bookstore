# --- !Ups
ALTER TABLE `account` ADD COLUMN `facebook_uid` VARCHAR(100) AFTER `is_lock`;
ALTER TABLE `account` ADD COLUMN `google_uid`   VARCHAR(100) AFTER `facebook_uid`;

# --- !Downs
ALTER TABLE `account` DROP COLUMN `facebook_uid`;
ALTER TABLE `account` DROP COLUMN `google_uid`;