# --- !Ups
CREATE TABLE account (
  account_name VARCHAR(40) NOT NULL PRIMARY KEY,
  account_password VARCHAR(40) NOT NULL,
  account_position VARCHAR(40) NOT NULL,
  account_avatar VARCHAR(500),
  is_lock TINYINT(1) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE staff (
  staff_id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  staff_name VARCHAR(100) NOT NULL,
  staff_acc_name VARCHAR(40) NOT NULL,
  staff_age INT(3) NOT NULL,
  staff_address VARCHAR(100),
  staff_phone VARCHAR(20),
  staff_salary INT NOT NULL,
  staff_note VARCHAR(500),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  created_by VARCHAR(40),
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  updated_by VARCHAR(40),
  CONSTRAINT fk_staff_account FOREIGN KEY (staff_acc_name) REFERENCES account(account_name)
);

CREATE TABLE customer (
  customer_id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  customer_name VARCHAR(100) NOT NULL,
  customer_acc_name VARCHAR(40) NOT NULL,
  customer_age INT(3) NOT NULL,
  customer_address VARCHAR(100),
  customer_phone VARCHAR(20),
  customer_type INT NOT NULL,
  customer_note VARCHAR(500),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT fk_customer_account FOREIGN KEY (customer_acc_name) REFERENCES account(account_name)
);

# --- !Downs
ALTER TABLE staff DROP FOREIGN KEY fk_staff_account;
ALTER TABLE customer DROP FOREIGN KEY fk_customer_account;
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS staff;
DROP TABLE IF EXISTS customer;
