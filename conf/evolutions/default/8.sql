# --- !Ups
CREATE TABLE order_history (
  order_history_id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  staff_id INT UNSIGNED NOT NULL,
  order_id INT UNSIGNED NOT NULL,
  order_history_status VARCHAR(100),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT fk_order_history_staff FOREIGN KEY (staff_id) REFERENCES staff(staff_id),
  CONSTRAINT fk_order_history_order FOREIGN KEY (order_id) REFERENCES `order`(order_id)
);

# --- !Downs
ALTER TABLE order_history DROP FOREIGN KEY fk_order_history_staff;
ALTER TABLE order_history DROP FOREIGN KEY fk_order_history_order;
DROP TABLE IF EXISTS order_history;
