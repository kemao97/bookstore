# --- !Ups
CREATE TABLE `order_detail` (
  order_detail_id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  order_id INT UNSIGNED NOT NULL,
  book_id INT UNSIGNED NOT NULL,
  book_cost INT NOT NULL,
  book_discount INT NOT NULL,
  book_number INT NOT NULL,
  CONSTRAINT fk_book_detail FOREIGN KEY (book_id) REFERENCES book(book_id),
  CONSTRAINT fk_order_detail FOREIGN KEY (order_id) REFERENCES `order`(order_id)
);

# --- !Downs
ALTER TABLE `order_detail` DROP FOREIGN KEY fk_book_detail;
ALTER TABLE `order_detail` DROP FOREIGN KEY fk_order_detail;
DROP TABLE IF EXISTS `order_detail`;
