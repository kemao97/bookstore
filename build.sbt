name := "bookstore"
 
version := "1.0" 
      
lazy val `bookstore` = (project in file(".")).enablePlugins(PlayJava)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
scalaVersion := "2.11.11"

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "5.1.36",
  "org.hibernate" % "hibernate-entitymanager" % "4.3.9.Final",
//  "org.hibernate" % "hibernate-jpamodelgen" % "4.3.6.Final",
  "com.adrianhurt" %% "play-bootstrap" % "1.0-P25-B3",
  "com.paypal.sdk" % "rest-api-sdk" % "1.14.0",
  "com.google.api-client" % "google-api-client" % "1.25.0",
  "com.google.apis" % "google-api-services-oauth2" % "v2-rev141-1.25.0",
  "com.restfb" % "restfb" % "2.3.0",
  evolutions,
  javaJpa,
  javaCore,
  cache,
  javaWs
)

//
//javaOptions ++= Seq("-Xmx512M", "-Xmx2048M", "-XX:MaxPermSize=2048M")
//
////for auto generating jpa model metadata
//javacOptions ++= Seq("-s", "app")