$('.content-top').css("min-height", screen.height - 485);

$(document).ready(function() {

    /**
     * integer number use to class input number
     */
    $(document).on('keydown', '.integer-character', function (e) {
        return e.keyCode !== 69 && e.keyCode !== 190 && e.keyCode !== 189 && ($(this).val().length < 10 || e.keyCode === 8);
    });

    /**
     * float number use to class input number
     */
    $(document).on('keydown', '.float-character', function (e) {
        return e.keyCode !== 69 && e.keyCode !== 45 && e.keyCode !== 189 && ($(this).val().length < 10 || e.keyCode === 8);
    });

    /**
     * show image name when choose from file explore
     */
    $(document).on('change', '#image', function () {
        var file = this.files[0];
        var filename = "";
        var current = $(this).attr("current") || "";
        if(current !== "") {
            filename = current;
        }
        if(file !== undefined) {
            filename = file.name;
        }
        $("#bookImage").val(filename);
    });

    /**
     * show user option
     */
    $(".account-name").click(function (e) {
        e.stopPropagation()
        var display = $(".user-option").css("display");
        if(display === "none") {
            $(".avatar").css("background-color", "#63a37b");
            $(".user-option").css("display", "block");
        }
    });

    /**
     * hidden user option
     */
    $(document).click(function () {
        if($(".user-option").css("display") == "block") {
            $(".user-option").css("display", "none");
            $(".avatar").css("background-color", "");
        }
    });

    /**
     * rotate image
     */
    $(".rotate-image").click(function (e) {
        var image = $(this).parent().children(":first-child");
        var imageName = image.attr("src").split('/')[3];
        $.ajax({
            type: "GET",
            url: '/book/rotate_image/' + imageName,
            processData: false,
            contentType: false
        });

        var deg = parseInt(image.attr('deg')) + 90;
        $(image).attr('deg', deg);
        $(image).css('transform', 'rotate(' + deg + 'deg)');
    });

    /**
     * zoom image and control rotate image of attach file
     */
    $('.img-select').click(function(env){
        // Get the modal
        var modal = document.getElementById('myPhotoModal');

        // Get the image and insert it inside the modal - use its "alt" text as a caption
        var divImage = $('#align-image');
        var modalImg = $("#img01");
        var captionText = document.getElementById("caption");

        modal.style.display = "block";
        modalImg.attr('src', $(this).attr('src'));


        var deg = $(this).attr('deg');
        //check image origin width > height?
        $.keyframe.define([{
            name: 'zoom',
            from: {'transform': 'scale(0)'},
            to: {'transform': 'scale(1)'}
        }]);
        var originImage = (parseFloat($(this).css('width')) - parseFloat($(this).css('height'))) * Math.pow(-1, deg % 2);
        var width = parseFloat(modalImg.css('width'));
        var height = parseFloat(modalImg.css('height'));
        if(originImage >= 0) {
            if((deg/90) % 4 == 1) {
                var space = width*width/2/height - width/2;
                modalImg.css('height', modalImg.css('width'));
                modalImg.css('width', 'auto');
                divImage.css('height', modalImg.css('width'));
                $.keyframe.define([{
                    name: 'zoom',
                    from: {'transform': 'scale(0) rotate(' + deg + 'deg)' + ' ' + 'translate(' + space + 'px, ' + space + 'px)'},
                    to: {'transform': 'scale(1) rotate(' + deg + 'deg)' + ' ' + 'translate(' + space + 'px, ' + space + 'px)'}
                }]);
            } else if((deg/90) % 4 == 3){
                var space = width*width/2/height - width/2;
                modalImg.css('height', modalImg.css('width'));
                modalImg.css('width', 'auto');
                divImage.css('height', modalImg.css('width'));
                $.keyframe.define([{
                    name: 'zoom',
                    from: {'transform': 'scale(0) rotate(' + deg + 'deg)' + ' ' + 'translate(' + -space + 'px, ' + -space + 'px)'},
                    to: {'transform': 'scale(1) rotate(' + deg + 'deg)' + ' ' + 'translate(' + -space + 'px, ' + -space + 'px)'}
                }]);
            } else {
                modalImg.css('width', '100%');
                modalImg.css('height', 'auto');
                divImage.css('height', '');
                $.keyframe.define([{
                    name: 'zoom',
                    from: {'transform': 'scale(0) rotate(' + deg + 'deg)'},
                    to: {'transform': 'scale(1) rotate(' + deg + 'deg)'}
                }]);
            }
        } else {
            if((deg/90) % 4 == 1) {
                var space = (width - width*width/height)/2;
                modalImg.css('height', modalImg.css('width'));
                modalImg.css('width', 'auto');
                divImage.css('height', (width*width/height).toString());
                $.keyframe.define([{
                    name: 'zoom',
                    from: {'transform': 'scale(0) rotate(' + deg + 'deg)' + ' ' + 'translate(' + -space + 'px, ' + 0 + 'px)'},
                    to: {'transform': 'scale(1) rotate(' + deg + 'deg)' + ' ' + 'translate(' + -space + 'px, ' + 0 + 'px)'}
                }]);
            } else if((deg/90) % 4 == 3){
                var space = (width - width*width/height)/2;
                modalImg.css('height', modalImg.css('width'));
                modalImg.css('width', 'auto');
                divImage.css('height', (width*width/height).toString());
                $.keyframe.define([{
                    name: 'zoom',
                    from: {'transform': 'scale(0) rotate(' + deg + 'deg)' + ' ' + 'translate(' + space + 'px, ' + 0 + 'px)'},
                    to: {'transform': 'scale(1) rotate(' + deg + 'deg)' + ' ' + 'translate(' + space + 'px, ' + 0 + 'px)'}
                }]);
            } else {
                modalImg.css('width', '100%');
                modalImg.css('height', 'auto');
                divImage.css('height', '');
                $.keyframe.define([{
                    name: 'zoom',
                    from: {'transform': 'scale(0) rotate(' + deg + 'deg)'},
                    to: {'transform': 'scale(1) rotate(' + deg + 'deg)'}
                }]);
            }
        }
        modalImg.playKeyframe({
            name: 'zoom',
            duration: '0.6s'
        });
        captionText.innerHTML = $(this).attr('alt');

        // Get the <span> element that closes the modal
        var span = document.getElementById("modelClose");

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
            modalImg.css('width', '100%');
            modalImg.css('height', 'auto');
            divImage.css('height', '');
            modalImg.css('transform', '');
        }
    });

    $('.book-number').change(function () {
        $.ajax({
            type: 'POST',
            url: $(this.form).attr('action'),
            data: new FormData($(this.form)[0]),
            cache: false,
            processData: false,
            contentType: false,
            success: function(response, textStatus, jqXHR) {
                alert("Thay đổi thành công");
                location.reload();
            },
            error: function (jqXHR) {
                alert("Có lỗi xảy ra");
            }
        });
    });

    $('.book-delete').click(function (e) {
        e.preventDefault();
        var result = confirm("Bạn có chắc chắn muốn bỏ sách khỏi giỏ hàng?");
        if(result) {
            var urlDelete = $(this.form).attr('action').replace('/edit/', '/delete/');
            $.get(urlDelete, function () {
                alert("Đã xoá thành công");
                location.reload();
            });
        }
    });

    $('.buy-book').click(function (e) {
        e.preventDefault();
        if($('.account-name h5').text() == "") {
            alert("Bạn cần đăng nhập trước khi mua hàng");
            return;
        }
        $.ajax({
            type: 'POST',
            url: $(this.form).attr('action'),
            data: new FormData($(this.form)[0]),
            cache: false,
            processData: false,
            contentType: false,
            success: function(response, textStatus, jqXHR) {
                alert("Thêm vào giỏ hàng thành công");
                $(".number-in-cart").text(response);
            },
            error: function (jqXHR) {
                if(jqXHR.status === 404) {
                    alert("Khách hàng chưa đăng nhập");
                } else {
                    alert("Sách đã có trong giỏ hàng");
                }
            }
        });
    })

    $('.button-order').click(function (e) {
        e.preventDefault();
        var result = confirm("Bạn đã kiểm tra kĩ đơn hàng? Địa điểm nhận hàng là địa chỉ của bạn");
        if(result && $()) {
            location.href = $(this).attr('href');
        }
    });

    $('.border').click(function () {
        $('#bookType').val($(this).children().attr('book-type'));
        $('#bookSearchForm').submit();
    });

    $('.btn-cancel-order').click(function (e) {
        e.preventDefault();
        var result = confirm("Bạn có chắc chắn muốn huỷ đơn hàng?");
        if(result) {
            var urlDelete = $(this).attr('href');
            $.get(urlDelete, function () {
                alert("Huỷ đơn hàng thành công");
                location.reload();
            }).fail(function () {
                alert("Đơn hàng không thể huỷ");
            });
        }
    });

    $('.btn-personal-edit').click(function () {
        $(this).parent().children('div').children().children().attr('readonly', false);
    });

    $('#personAvatarInput').change(function () {
        var file = this.files[0];
        var fileName = file.name;
        $.ajax({
            type: 'POST',
            url: $(this.form).attr('action'),
            data: new FormData($(this.form)[0]),
            cache: false,
            processData: false,
            contentType: false,
            success: function(response, textStatus, jqXHR) {
                $('#personAvatar').attr('src', response);
                $('#avatarSource').val(response);
                $('#avatarName').val(fileName);
            },
            error: function (jqXHR) {
                alert("Vui lòng upload ảnh");
            }
        });
    });
});
