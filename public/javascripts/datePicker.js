/**
 * date picker for all page and modal
 */
var datePicker = function () {
    $('#enterDatePicker').datetimepicker({
        format: 'YYYY年MM月DD日',
        locale: 'ja'
    });

    $('#retireDatePicker').datetimepicker({
        format: 'YYYY年MM月DD日',
        locale: 'ja'
    });

    $('#birthdatePicker').datetimepicker({
        format: 'YYYY年MM月DD日',
        locale: 'ja'
    });

    $('#paymentDate').datetimepicker({
        format: 'YYYY年MM月DD日',
        locale: 'ja'
    });

    $('#deadlineDate').datetimepicker({
        format: 'YYYY年MM月DD日',
        locale: 'ja'
    });

    $('#normal_picker').datetimepicker({
        format: 'YYYY年MM月DD日',
        locale: 'ja'
    });
    
    $('#date_picker').datetimepicker({
        format: 'YYYY年MM月DD日',
        locale: 'ja'
    });
};

datePicker();