$(document).ready(function() {
    $('.close').click(function () {
        $(this).parent().css('display', 'none');
    });

    function resetModalAlert() {
        $('#alertContentError').css('display', 'none');
        $('#alertContentInfo').css('display', 'none');
        $('#alertContentSuccess').css('display', 'none');
    }

    $('.btn-add-view').click(function (e) {
        e.preventDefault();
        resetModalAlert();
        $(".modal-title").empty();
        $(".modal-body-content").empty();
        var title = $(this).attr('modal-title');
        $.ajax({
            type: 'GET',
            url: $(this).attr('href'),
            async: false,
            success: function(data, status) {
                if(status == "success") {
                    $(".modal-title").text(title);
                    $(".modal-body-content").append(data);
                    $('#myModal').modal('show');
                    datePicker();
                }
            }
        });
    });

    $(document).on("click", ".btn-add" , function(e) {
        e.preventDefault();
        resetModalAlert();
        var title = $(this).attr('modal-title');
        var load = $(this).attr("href");
        $.ajax({
            type: 'POST',
            url: $(this.form).attr('action'),
            data: new FormData($(this.form)[0]),
            cache: false,
            processData: false,
            contentType: false,
            success: function(response, textStatus, jqXHR) {
                if(load !== undefined) {
                    window.location.href = load;
                    alert("Đăng kí thành công");
                    return;
                }
                $(".modal-title").empty();
                $(".modal-body-content").empty();
                if (textStatus === "success") {
                    $(".modal-title").text(title);
                    $(".modal-body-content").append(response);
                }
                $('#alertContentSuccess').css('display', 'block');
                $('#alertContentSuccess strong').text("Đăng kí thành công");
                datePicker();
            },
            error: function (jqXHR) {
                if (jqXHR.status == 400) {
                    $(".modal-body-content").empty();
                    $(".modal-body-content").append(jqXHR.responseText);
                    $('#alertContentError strong').text("Đăng kí không thành công");
                }
                $('#bookImage').val('');
                $('#alertContentError').css('display', 'block');
                datePicker();
            }
        });
    });

    $('.btn-edit-view').click(function (e) {
        e.preventDefault();
        resetModalAlert();
        $(".modal-title").empty();
        $(".modal-body-content").empty();
        var title = $(this).attr('modal-title');
        $.ajax({
            type: 'GET',
            url: $(this).attr('href'),
            async: false,
            success: function(data, status) {
                if(status == "success") {
                    $(".modal-title").text(title);
                    $(".modal-body-content").append(data);
                    $('#myModal').modal('show');
                    datePicker();
                }
            }
        });
    });

    $(document).on("click", ".btn-edit" , function(e) {
        e.preventDefault();
        resetModalAlert();
        var title = $(this).attr('modal-title');
        var load = $(this).attr("href");
        $.ajax({
            type: 'POST',
            url: $(this.form).attr('action'),
            data: new FormData($(this.form)[0]),
            cache: false,
            processData: false,
            contentType: false,
            success: function(response, textStatus, jqXHR) {
                if(load !== undefined) {
                    window.location.href = load;
                    alert("Đổi mật khẩu thành công");
                    return;
                }
                $(".modal-title").empty();
                $(".modal-body-content").empty();
                if (textStatus === "success") {
                    $(".modal-title").text(title);
                    $(".modal-body-content").append(response);
                };
                $('#alertContentInfo').css('display', 'block');
                $('#alertContentInfo strong').text("Cập nhật thành công");
                datePicker();
            },
            error: function (jqXHR) {
                if (jqXHR.status == 400) {
                    $(".modal-body-content").empty();
                    $(".modal-body-content").append(jqXHR.responseText);
                };
                $('#alertContentError').css('display', 'block');
                $('#alertContentError strong').text("cập nhật không thành công");
                $('#bookImage').val($('#image').attr('current'));
                datePicker();
            }
        });
    });

    $(document).on("click", ".btn-sign-in" , function(e) {
        e.preventDefault();
        resetModalAlert();
        $.ajax({
            type: 'POST',
            url: $(this.form).attr('action'),
            data: new FormData($(this.form)[0]),
            cache: false,
            processData: false,
            contentType: false,
            success: function(response, textStatus, jqXHR) {
                location.reload();
                return;
            },
            error: function (jqXHR) {
                if (jqXHR.status == 400) {
                    $(".modal-body-content").empty();
                    $(".modal-body-content").append(jqXHR.responseText);
                    $('#alertContentError strong').text("Đăng nhập không thành công");
                } else if(jqXHR.status === 404) {
                    $(".modal-body-content").empty();
                    $(".modal-body-content").append(jqXHR.responseText);
                    $('#alertContentError strong').text("Tài khoản đã bị khoá");
                } else if(jqXHR.status == 500) {
                    $(".modal-body-content").empty();
                    $(".modal-body-content").append(jqXHR.responseText);
                    $('#alertContentError strong').text("Tên tài khoản hoặc mật khẩu không chính xác");
                }
                $('#alertContentError').css('display', 'block');
                datePicker();
            }
        });
    });

    /**
     * lock account
     */
    $(document).on('click', '.btn-lock', function (e) {
        e.preventDefault();
        $.get($(this).attr('href'), function () {
            alert("Tài khoản đã bị khoá");
            location.reload();
        }).fail(function () {
            alert("Tài khoản không tồn tại");
        });
    });

    /**
     * unlock account
     */
    $(document).on('click', '.btn-unlock', function (e) {
        e.preventDefault();
        $.get($(this).attr('href'), function () {
            alert("Tài khoản đã được mở khoá");
            location.reload();
        }).fail(function () {
            alert("Tài khoản không tồn tại");
        });
    });

    /**
     * reload after close modal add/edit
     */
    $('.btn-reload').click(function () {
        location.reload();
    });

    /**
     * fix scroll problem when second modal is hidden
     */

    $('.modal').on('hidden.bs.modal', function (e) {
        if($('.modal').hasClass('in')) {
            $('body').addClass('modal-open');
        }
    });

    /**
     * this function will close modal
     * */
    $(document).on('click', '.modal-close', function () {
        var modalId = $(this).parent().parent().parent().parent().attr("id");
        $('#'+modalId).modal('toggle');
    });
});
