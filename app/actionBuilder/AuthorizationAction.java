package actionBuilder;

import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import utils.GlobalVariable;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class AuthorizationAction extends Action<Authorization> {

  @Override
  public CompletionStage<Result> call(Http.Context ctx) {
    CompletionStage<Result> result = delegate.call(ctx);
    String position = ctx.session().get(GlobalVariable.userPosition);
    List<String> args = Arrays.asList(configuration.value());

    boolean signInArgs = args.contains(GlobalVariable.signIn);
    boolean notSignInArgs = args.contains(GlobalVariable.notSignIn);

    if (
      (signInArgs && notSignInArgs) || (signInArgs && position == null) ||
      (notSignInArgs && position != null) || (!signInArgs && !notSignInArgs && !args.contains(position))
    ){
      result = CompletableFuture.supplyAsync(() -> redirect(controllers.routes.Application.index()));
    }

    return result;
  }

}
