package forms;

import play.data.validation.Constraints;

public class BookSearchForm {

  @Constraints.MaxLength(100)
  private String bookName;

  private String bookType;

  public BookSearchForm() {}

  public BookSearchForm(String bookName, String bookType) {
    this.bookName = bookName;
    this.bookType = bookType;
  }

  public String getBookName() {
    return bookName;
  }

  public void setBookName(String bookName) {
    this.bookName = bookName;
  }

  public String getBookType() {
    return bookType;
  }

  public void setBookType(String bookType) {
    this.bookType = bookType;
  }
}
