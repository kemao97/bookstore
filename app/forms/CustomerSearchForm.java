package forms;

import play.data.validation.Constraints;

public class CustomerSearchForm {

  @Constraints.MaxLength(100)
  private String accountName;

  @Constraints.MaxLength(100)
  private String customerName;

  public CustomerSearchForm() {}

  public CustomerSearchForm(String accountName, String customerName) {
    this.accountName = accountName;
    this.customerName = customerName;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }
}
