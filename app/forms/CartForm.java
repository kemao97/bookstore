package forms;

import play.data.validation.Constraints;

public class CartForm {

  @Constraints.Required
  private int bookId;

  private int bookNumber = 1;

  public CartForm() {}

  public CartForm(int bookId, int bookNumber) {
    this.bookId = bookId;
    this.bookNumber = bookNumber;
  }

  public int getBookId() {
    return bookId;
  }

  public void setBookId(int bookId) {
    this.bookId = bookId;
  }

  public int getBookNumber() {
    return bookNumber;
  }

  public void setBookNumber(int bookNumber) {
    this.bookNumber = bookNumber;
  }
}
