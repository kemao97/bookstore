package forms;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class CustomerForm {

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String customerName;

  @Constraints.Required
  private int customerAge;

  @Constraints.MaxLength(100)
  private String customerAddress;

  @Constraints.MaxLength(20)
  private String customerPhone;

  @Constraints.MaxLength(500)
  private String customerNote;

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String accountName;

  @Constraints.Required
  @Constraints.MaxLength(40)
  private String accountPassword;

  @Constraints.Required
  @Constraints.MaxLength(40)
  private String accountRepeatPassword;

  public CustomerForm() {}

  public CustomerForm(String customerName, int customerAge, String customerAddress, String customerPhone, String customerNote, String accountName, String accountPassword, String accountRepeatPassword) {
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerAddress = customerAddress;
    this.customerPhone = customerPhone;
    this.customerNote = customerNote;
    this.accountName = accountName;
    this.accountPassword = accountPassword;
    this.accountRepeatPassword = accountRepeatPassword;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public int getCustomerAge() {
    return customerAge;
  }

  public void setCustomerAge(int customerAge) {
    this.customerAge = customerAge;
  }

  public String getCustomerAddress() {
    return customerAddress;
  }

  public void setCustomerAddress(String customerAddress) {
    this.customerAddress = customerAddress;
  }

  public String getCustomerPhone() {
    return customerPhone;
  }

  public void setCustomerPhone(String customerPhone) {
    this.customerPhone = customerPhone;
  }

  public String getCustomerNote() {
    return customerNote;
  }

  public void setCustomerNote(String customerNote) {
    this.customerNote = customerNote;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getAccountPassword() {
    return accountPassword;
  }

  public void setAccountPassword(String accountPassword) {
    this.accountPassword = accountPassword;
  }

  public String getAccountRepeatPassword() {
    return accountRepeatPassword;
  }

  public void setAccountRepeatPassword(String accountRepeatPassword) {
    this.accountRepeatPassword = accountRepeatPassword;
  }

  public List<ValidationError> validate() {
    List<ValidationError> errors = new ArrayList<ValidationError>();
    if(customerAge < 0 || customerAge > 300) {
      errors.add(new ValidationError("customerAge", "customer.age"));
    }
    if(!accountPassword.equals(accountRepeatPassword)) {
      errors.add(new ValidationError("accountRepeatPassword", "customer.repeatPassword"));
    }
    return errors.isEmpty() ? null : errors;
  }
}
