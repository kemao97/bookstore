package forms;

import play.data.validation.Constraints;

public class OrderSearchForm {

  @Constraints.MaxLength(100)
  private String customerName;

  private String orderStatus;

  public OrderSearchForm() {}

  public OrderSearchForm(String customerName, String customerStatus) {
    this.customerName = customerName;
    this.orderStatus = customerStatus;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }
}
