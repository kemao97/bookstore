package forms;

import play.data.validation.Constraints;

public class PersonForm {
  @Constraints.Required
  @Constraints.MaxLength(100)
  private String personName;

  @Constraints.Required
  private int personAge;

  @Constraints.MaxLength(100)
  private String personAddress;

  @Constraints.MaxLength(11)
  private String personPhone;

  @Constraints.MaxLength(500)
  private String personNote;

  private String avatarSource;

  private String avatarName;

  public PersonForm() {
  }

  public PersonForm(String personName, int personAge, String personAddress, String personPhone, String personNote) {
    this.personName = personName;
    this.personAge = personAge;
    this.personAddress = personAddress;
    this.personPhone = personPhone;
    this.personNote = personNote;
  }

  public PersonForm(String personName, int personAge, String personAddress, String personPhone, String personNote, String avatarSource, String avatarName) {
    this.personName = personName;
    this.personAge = personAge;
    this.personAddress = personAddress;
    this.personPhone = personPhone;
    this.personNote = personNote;
    this.avatarSource = avatarSource;
    this.avatarName = avatarName;
  }

  public String getPersonName() {
    return personName;
  }

  public void setPersonName(String personName) {
    this.personName = personName;
  }

  public int getPersonAge() {
    return personAge;
  }

  public void setPersonAge(int personAge) {
    this.personAge = personAge;
  }

  public String getPersonAddress() {
    return personAddress;
  }

  public void setPersonAddress(String personAddress) {
    this.personAddress = personAddress;
  }

  public String getPersonPhone() {
    return personPhone;
  }

  public void setPersonPhone(String personPhone) {
    this.personPhone = personPhone;
  }

  public String getPersonNote() {
    return personNote;
  }

  public void setPersonNote(String personNote) {
    this.personNote = personNote;
  }

  public String getAvatarSource() {
    return avatarSource;
  }

  public void setAvatarSource(String avatarSource) {
    this.avatarSource = avatarSource;
  }

  public String getAvatarName() {
    return avatarName;
  }

  public void setAvatarName(String avatarName) {
    this.avatarName = avatarName;
  }
}
