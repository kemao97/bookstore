package forms;

import play.data.validation.Constraints;

public class StaffSearchForm {

  @Constraints.MaxLength(100)
  private String accountName;

  @Constraints.MaxLength(100)
  private String staffName;

  private String position;

  public StaffSearchForm() {}

  public StaffSearchForm(String accountName, String staffName, String position) {
    this.accountName = accountName;
    this.staffName = staffName;
    this.position = position;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getStaffName() {
    return staffName;
  }

  public void setStaffName(String userName) {
    this.staffName = userName;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }
}
