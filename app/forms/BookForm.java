package forms;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class BookForm {

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String bookName;

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String bookType;

  @Constraints.Required
  private int bookCost;

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String bookAuthor;

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String bookProduce;

  @Constraints.Required
  private int bookDiscount;

  @Constraints.MaxLength(500)
  private String bookNote;

  @Constraints.Required
  @Constraints.MaxLength(500)
  private String bookImage;

  @Constraints.Required
  private boolean isLock;

  public BookForm() {}

  public BookForm(String bookName, String bookType, int bookCost, String bookAuthor, String bookProduce, int bookDiscount, String bookNote, String bookImage, boolean isLock) {
    this.bookName = bookName;
    this.bookType = bookType;
    this.bookCost = bookCost;
    this.bookAuthor = bookAuthor;
    this.bookProduce = bookProduce;
    this.bookDiscount = bookDiscount;
    this.bookNote = bookNote;
    this.bookImage = bookImage;
    this.isLock = isLock;
  }

  public String getBookName() {
    return bookName;
  }

  public void setBookName(String bookName) {
    this.bookName = bookName;
  }

  public String getBookType() {
    return bookType;
  }

  public void setBookType(String bookType) {
    this.bookType = bookType;
  }

  public int getBookCost() {
    return bookCost;
  }

  public void setBookCost(int bookCost) {
    this.bookCost = bookCost;
  }

  public String getBookAuthor() {
    return bookAuthor;
  }

  public void setBookAuthor(String bookAuthor) {
    this.bookAuthor = bookAuthor;
  }

  public String getBookProduce() {
    return bookProduce;
  }

  public void setBookProduce(String bookProduce) {
    this.bookProduce = bookProduce;
  }

  public int getBookDiscount() {
    return bookDiscount;
  }

  public void setBookDiscount(int bookDiscount) {
    this.bookDiscount = bookDiscount;
  }

  public String getBookNote() {
    return bookNote;
  }

  public void setBookNote(String bookNote) {
    this.bookNote = bookNote;
  }

  public String getBookImage() {
    return bookImage;
  }

  public void setBookImage(String bookImage) {
    this.bookImage = bookImage;
  }

  public boolean isLock() {
    return isLock;
  }

  public void setLock(boolean lock) {
    isLock = lock;
  }

  public List<ValidationError> validate() {
    List<ValidationError> errors = new ArrayList<ValidationError>();
    if(
      !(bookImage.endsWith(".jpeg") || bookImage.endsWith(".JPEG") ||
      bookImage.endsWith(".jpg") ||  bookImage.endsWith(".JPG") ||
      bookImage.endsWith(".png") ||  bookImage.endsWith(".PNG") ||
      bookImage.endsWith(".gif") ||  bookImage.endsWith(".GIF") ||
      bookImage.endsWith(".webp") || bookImage.endsWith(".WEBP") ||
      bookImage.endsWith(".tif") ||  bookImage.endsWith(".TIF") ||
      bookImage.endsWith(".bmp") ||  bookImage.endsWith(".BMP") ||
      bookImage.endsWith(".jxr") ||  bookImage.endsWith(".JXR") ||
      bookImage.endsWith(".psd") ||  bookImage.endsWith(".PSD")
      )
    ) {
      errors.add(new ValidationError("bookImage", "book.bookImageValid"));
    }
    if(bookDiscount < 0 || bookDiscount > 100) {
      errors.add(new ValidationError("bookDiscount", "book.bookDiscount"));
    }
    return errors.isEmpty() ? null : errors;
  }
}
