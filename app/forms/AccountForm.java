package forms;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class AccountForm {

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String accountName;

  @Constraints.Required
  @Constraints.MaxLength(40)
  private String accountPassword;

  @Constraints.Required
  private String accountPosition = "customer";

  public AccountForm() {}

  public AccountForm(String accountName, String accountPassword, String accountPosition) {
    this.accountName = accountName;
    this.accountPassword = accountPassword;
    this.accountPosition = accountPosition;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getAccountPassword() {
    return accountPassword;
  }

  public void setAccountPassword(String accountPassword) {
    this.accountPassword = accountPassword;
  }

  public String getAccountPosition() {
    return accountPosition;
  }

  public void setAccountPosition(String accountPosition) {
    this.accountPosition = accountPosition;
  }

  public List<ValidationError> validate() {
    List<ValidationError> errors = new ArrayList<ValidationError>();


    return errors.isEmpty() ? null : errors;
  }
}
