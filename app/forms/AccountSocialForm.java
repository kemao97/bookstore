package forms;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class AccountSocialForm {

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String accountName;

  @Constraints.Required
  @Constraints.MaxLength(40)
  private String accountPassword;

  public AccountSocialForm() {}

  public AccountSocialForm(String accountName, String accountPassword, String accountPosition) {
    this.accountName = accountName;
    this.accountPassword = accountPassword;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getAccountPassword() {
    return accountPassword;
  }

  public void setAccountPassword(String accountPassword) {
    this.accountPassword = accountPassword;
  }

  public List<ValidationError> validate() {
    List<ValidationError> errors = new ArrayList<ValidationError>();

    return errors.isEmpty() ? null : errors;
  }
}
