package forms;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class StaffForm {

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String staffName;

  @Constraints.Required
  private int staffAge;

  @Constraints.MaxLength(100)
  private String staffAddress;

  @Constraints.MaxLength(11)
  private String staffPhone;

  @Constraints.Required
  @Constraints.Min(0)
  private int staffSalary;

  @Constraints.MaxLength(500)
  private String staffNote;

  @Constraints.Required
  @Constraints.MaxLength(40)
  private String accountName;

  @Constraints.Required
  @Constraints.MaxLength(40)
  private String accountPassword;

  @Constraints.Required
  @Constraints.MaxLength(40)
  private String accountRepeatPassword;

  @Constraints.Required
  @Constraints.MaxLength(40)
  private String accountPosition;

  public StaffForm() {}

  public StaffForm(String staffName, int staffAge, String staffAddress, String staffPhone, int staffSalary, String staffNote, String accountName, String accountPassword, String accountRepeatPassword, String accountPosition) {
    this.staffName = staffName;
    this.staffAge = staffAge;
    this.staffAddress = staffAddress;
    this.staffPhone = staffPhone;
    this.staffSalary = staffSalary;
    this.staffNote = staffNote;
    this.accountName = accountName;
    this.accountPassword = accountPassword;
    this.accountRepeatPassword = accountRepeatPassword;
    this.accountPosition = accountPosition;
  }

  public String getStaffName() {
    return staffName;
  }

  public void setStaffName(String staffName) {
    this.staffName = staffName;
  }

  public int getStaffAge() {
    return staffAge;
  }

  public void setStaffAge(int staffAge) {
    this.staffAge = staffAge;
  }

  public String getStaffAddress() {
    return staffAddress;
  }

  public void setStaffAddress(String staffAddress) {
    this.staffAddress = staffAddress;
  }

  public String getStaffPhone() {
    return staffPhone;
  }

  public void setStaffPhone(String staffPhone) {
    this.staffPhone = staffPhone;
  }

  public int getStaffSalary() {
    return staffSalary;
  }

  public void setStaffSalary(int staffSalary) {
    this.staffSalary = staffSalary;
  }

  public String getStaffNote() {
    return staffNote;
  }

  public void setStaffNote(String staffNote) {
    this.staffNote = staffNote;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getAccountPassword() {
    return accountPassword;
  }

  public void setAccountPassword(String accountPassword) {
    this.accountPassword = accountPassword;
  }

  public String getAccountPosition() {
    return accountPosition;
  }

  public void setAccountPosition(String accountPosition) {
    this.accountPosition = accountPosition;
  }

  public String getAccountRepeatPassword() {
    return accountRepeatPassword;
  }

  public void setAccountRepeatPassword(String accountRepeatPassword) {
    this.accountRepeatPassword = accountRepeatPassword;
  }

  public List<ValidationError> validate() {
    List<ValidationError> errors = new ArrayList<ValidationError>();
    if(staffAge < 0 || staffAge > 300) {
      errors.add(new ValidationError("staffAge", "staff.age"));
    }
    if(staffSalary < 0 || staffSalary > 1000000000) {
      errors.add(new ValidationError("staffSalary", "staff.salary"));
    }
    if(!accountPassword.equals(accountRepeatPassword)) {
      errors.add(new ValidationError("accountRepeatPassword", "staff.repeatPassword"));
    }
    return errors.isEmpty() ? null : errors;
  }
}
