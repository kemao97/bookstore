package forms;

import play.data.validation.Constraints;
import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class PasswordForm {

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String oldPassword;

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String newPassword;

  @Constraints.Required
  @Constraints.MaxLength(100)
  private String reNewPassword;

  public PasswordForm() {
  }

  public PasswordForm(String oldPassword, String newPassword, String reNewPassword) {
    this.oldPassword = oldPassword;
    this.newPassword = newPassword;
    this.reNewPassword = reNewPassword;
  }

  public String getOldPassword() {
    return oldPassword;
  }

  public void setOldPassword(String oldPassword) {
    this.oldPassword = oldPassword;
  }

  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }

  public String getReNewPassword() {
    return reNewPassword;
  }

  public void setReNewPassword(String reNewPassword) {
    this.reNewPassword = reNewPassword;
  }

  public List<ValidationError> validate() {
    List<ValidationError> errors = new ArrayList<ValidationError>();
    if(!newPassword.equals(reNewPassword)) {
      errors.add(new ValidationError("reNewPassword", "staff.repeatPassword"));
    }
    return errors.isEmpty() ? null : errors;
  }
}
