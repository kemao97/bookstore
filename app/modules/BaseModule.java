package modules;

import com.google.inject.AbstractModule;
import models.daos.*;
import models.services.*;

public class BaseModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(CustomerService.class).to(CustomerServiceImpl.class);
    bind(CustomerDAO.class).to(CustomerDAOImpl.class);
    bind(AccountService.class).to(AccountServiceImpl.class);
    bind(AccountDAO.class).to(AccountDAOImpl.class);
    bind(StaffService.class).to(StaffServiceImpl.class);
    bind(StaffDAO.class).to(StaffDAOImpl.class);
    bind(BookService.class).to(BookServiceImpl.class);
    bind(BookDAO.class).to(BookDAOImpl.class);
    bind(CartService.class).to(CartServiceImpl.class);
    bind(CartDAO.class).to(CartDAOImpl.class);
    bind(OrderService.class).to(OrderServiceImpl.class);
    bind(OrderDAO.class).to(OrderDAOImpl.class);
    bind(OrderDetailService.class).to(OrderDetailServiceImpl.class);
    bind(OrderDetailDAO.class).to(OrderDetailDAOImpl.class);
    bind(OrderHistoryService.class).to(OrderHistoryServiceImpl.class);
    bind(OrderHistoryDAO.class).to(OrderHistoryDAOImpl.class);
  }
}
