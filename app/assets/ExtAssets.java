package assets;

import play.Environment;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.File;
import java.net.URLDecoder;

public class ExtAssets extends Controller {

  private Environment environment;

  @Inject
  public ExtAssets(Environment environment) {
    this.environment = environment;
  }

  public Result at(String rootPath, String fileName) {
    Result result = notFound();
    try {
      File fileToServe = new File(
        environment.rootPath() + "/" + rootPath + "/" + URLDecoder.decode(fileName, "UTF-8")
      );
      if(fileToServe.exists()) {
        result = ok().sendFile(fileToServe, true);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return result;
  }
}
