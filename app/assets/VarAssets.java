package assets;

import play.Environment;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.File;
import java.net.URLDecoder;

public class VarAssets extends Controller {

  private Environment environment;

  @Inject
  public VarAssets(Environment environment) {
    this.environment = environment;
  }

  public Result at(String rootPath, String fileName) {
    Result result = notFound();
    try {
      System.out.println(rootPath + "/" + fileName);
      File fileToServe = new File(
        rootPath + "/" + fileName
      );
      if(fileToServe.exists()) {
        result = ok().sendFile(fileToServe, true);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return result;
  }
}
