package controllers;

import actionBuilder.Authorization;
import com.fasterxml.jackson.databind.JsonNode;
import forms.BookForm;
import forms.BookSearchForm;
import javafx.util.Pair;
import models.entities.Account;
import models.entities.Book;
import models.entities.Customer;
import models.entities.Staff;
import models.services.*;
import play.Environment;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utils.GlobalVariable;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import utils.Convert;

public class BookController extends Controller {

  private HttpExecutionContext ec;
  private BookService bookService;
  private FormFactory formFactory;
  private AccountService accountService;
  private Environment environment;
  private CustomerService customerService;
  private StaffService staffService;
  private CartService cartService;

  private int LIMIT = 5;
  private int BOOK_SEARCH_LIMIT = 16;

  @Inject
  public BookController(HttpExecutionContext ec, BookService bookService, FormFactory formFactory, AccountService accountService, Environment environment, CustomerService customerService, StaffService staffService, CartService cartService) {
    this.ec = ec;
    this.bookService = bookService;
    this.formFactory = formFactory;
    this.accountService = accountService;
    this.environment = environment;
    this.customerService = customerService;
    this.staffService = staffService;
    this.cartService = cartService;
  }

  @Authorization({"admin", "manager", "importer"})
  public CompletionStage<Result> search(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      BookSearchForm bookSearchData = formFactory.form(BookSearchForm.class).bindFromRequest().get();
      String searchSession = Json.asciiStringify(Json.toJson(bookSearchData));
      session("bookSearchForm", searchSession);

      Pair<Integer, List<Book>> bookList = bookService.search(bookSearchData, offset, LIMIT);
      int totalPage = 0;
      if(bookList.getKey() != 0) {
        totalPage = (bookList.getKey() - 1) / LIMIT + 1;
      }
      Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));

      return ok(views.html.book.index.render(
        staff.getStaffName(),
        formFactory.form(BookSearchForm.class).bindFromRequest(),
        bookList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"admin", "manager", "importer"})
  public CompletionStage<Result> searchView(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      String bookSearchJson = session("bookSearchForm");
      BookSearchForm bookSearch;

      if(bookSearchJson == null) {
        bookSearch = new BookSearchForm(
          "",
          ""
        );
      } else {
        JsonNode json = Json.parse(bookSearchJson);
        bookSearch = Json.fromJson(json, BookSearchForm.class);
      }

      Pair<Integer, List<Book>> bookList = bookService.search(bookSearch, offset, LIMIT);
      int totalPage = 0;
      if(bookList.getKey() != 0) {
        totalPage = (bookList.getKey() - 1) / LIMIT + 1;
      }

      Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));

      return ok(views.html.book.index.render(
        staff.getStaffName(),
        formFactory.form(BookSearchForm.class).fill(bookSearch),
        bookList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  public CompletionStage<Result> bookSearch(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      BookSearchForm bookSearchData = formFactory.form(BookSearchForm.class).bindFromRequest().get();
      String searchSession = Json.asciiStringify(Json.toJson(bookSearchData));
      session("bookSearch", searchSession);

      Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
      int cartCount = 0;
      String fullName = "";
      if(customer != null) {
        cartCount = cartService.count(customer.getCustomerId());
        fullName = customer.getCustomerName();
      } else {
        if(session(GlobalVariable.userPosition) != null) {
          fullName = staffService.findByAccount(session(GlobalVariable.userIdentity)).getStaffName();
        }
      }

      Pair<Integer, List<Book>> bookList = bookService.bookSearch(bookSearchData, offset, BOOK_SEARCH_LIMIT);
      int totalPage = 0;
      if(bookList.getKey() != 0) {
        totalPage = (bookList.getKey() - 1) / BOOK_SEARCH_LIMIT + 1;
      }

      return ok(views.html.book.search.render(
        fullName,
        bookList.getValue(),
        cartCount,
        offset,
        totalPage
      ));
    }, ec.current());
  }

  public CompletionStage<Result> bookSearchView(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      String bookSearchJson = session("bookSearch");
      BookSearchForm bookSearch;

      if(bookSearchJson == null) {
        bookSearch = new BookSearchForm(
          "",
          ""
        );
      } else {
        JsonNode json = Json.parse(bookSearchJson);
        bookSearch = Json.fromJson(json, BookSearchForm.class);
      }

      Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
      int cartCount = 0;
      String fullName = "";
      if(customer != null) {
        cartCount = cartService.count(customer.getCustomerId());
        fullName = customer.getCustomerName();
      } else {
        if(session(GlobalVariable.userPosition) != null) {
          fullName = staffService.findByAccount(session(GlobalVariable.userIdentity)).getStaffName();
        }
      }

      Pair<Integer, List<Book>> bookList = bookService.bookSearch(bookSearch, offset, BOOK_SEARCH_LIMIT);
      int totalPage = 0;
      if(bookList.getKey() != 0) {
        totalPage = (bookList.getKey() - 1) / BOOK_SEARCH_LIMIT + 1;
      }

      return ok(views.html.book.search.render(
        fullName,
        bookList.getValue(),
        cartCount,
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"admin", "manager", "importer"})
  public CompletionStage<Result> addView() {
    return CompletableFuture.supplyAsync(() ->
      ok(views.html.book.add.render(formFactory.form(BookForm.class))), ec.current()
    );
  }

  @Authorization({"admin", "manager", "importer"})
  public CompletionStage<Result> add() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Form<BookForm> bookForm = formFactory.form(BookForm.class).bindFromRequest();

      if(bookForm.hasErrors()) {
        result = badRequest(views.html.book.add.render(bookForm));
      } else {
        Http.MultipartFormData.FilePart<Object> filePart = request().body().asMultipartFormData().getFile("image");
        if(filePart != null) {

          if (bookForm.hasErrors()) {
            result = badRequest(views.html.book.add.render(bookForm));
          } else {
            File fileUpload = (File) filePart.getFile();
            File fileSave = fileSave(filePart.getFilename());
            fileUpload.renameTo(fileSave);
            Account accountRetrieve = accountService.retrieve(session(GlobalVariable.userIdentity));
            BookForm data = bookForm.get();
            Book book = new Book(
              data.getBookName(),
              data.getBookType(),
              data.getBookCost(),
              data.getBookAuthor(),
              data.getBookProduce(),
              data.getBookDiscount(),
              data.getBookNote(),
              "/ext/bookImage/" + fileSave.getName(),
              false,
              GlobalVariable.currentTimestamp,
              GlobalVariable.currentTimestamp,
              accountRetrieve,
              accountRetrieve
            );

            int bookId = bookService.save(book);
            result = redirect(routes.BookController.editView(bookId));

          }
        } else {
          bookForm.reject("bookImage", "book.bookImageExist");
          result = badRequest(views.html.book.add.render(formFactory.form(BookForm.class)));
        }
      }
      return result;
    }, ec.current());
  }

  @Authorization({"admin", "manager", "importer"})
  public CompletionStage<Result> rotateImage(String fileName) {
    return CompletableFuture.supplyAsync(() -> {
      String formatName = fileName.split("\\.")[fileName.split("\\.").length - 1];
      File rootPath = environment.rootPath();
      File imageFile = new File(rootPath + "/upload/bookImage/" + fileName);
      if(imageFile.exists()) {
        try {
          BufferedImage image = ImageIO.read(imageFile);
          int width = image.getWidth();
          int height = image.getHeight();
          BufferedImage newImage = new BufferedImage(height, width, image.getType());
          Graphics2D g2d = newImage.createGraphics();
          g2d.rotate(Math.PI / 2);
          g2d.drawImage(image, 0, -height, null);
          ImageIO.write(newImage, formatName, imageFile);
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
      return ok();
    }, ec.current());
  }

  @Authorization({"admin", "manager", "importer"})
  public CompletionStage<Result> editView(int bookId) {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Book book = bookService.retrieve(bookId);
      String bookImageName = "";
      try {
        bookImageName = Convert.last(book.getBookImage().split("/"));
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      if(book != null) {
        BookForm bookForm = new BookForm(
          book.getBookName(),
          book.getBookType(),
          book.getBookCost(),
          book.getBookAuthor(),
          book.getBookProduce(),
          book.getBookDiscount(),
          book.getBookNote(),
          bookImageName,
          book.isLock()
        );
        result = ok(views.html.book.edit.render(
          formFactory.form(BookForm.class).fill(bookForm),
          book
        ));
      } else {
        result = redirect(routes.BookController.searchView(1));
      }

      return result;
    }, ec.current());
  }

  @Authorization({"admin", "manager", "importer"})
  public CompletionStage<Result> edit(int bookId) {
    return CompletableFuture.supplyAsync(() -> {
      Result result;

      Book book = bookService.retrieve(bookId);
      Form<BookForm> bookForm = formFactory.form(BookForm.class).bindFromRequest();
      if(book != null) {
        if(bookForm.hasErrors()) {
          result = badRequest(views.html.book.edit.render(bookForm, book));
        } else {
          BookForm data = bookForm.get();
          Http.MultipartFormData.FilePart<Object> filePart = request().body().asMultipartFormData().getFile("image");
          if(((File)filePart.getFile()).length() != 0) {
            File imageOld = new File(environment.rootPath() + book.getBookImage().replace("/ext/", "/upload/"));
            if(imageOld.exists()) {
              imageOld.delete();
            }
            File fileUpload = (File) filePart.getFile();
            File fileSave = fileSave(filePart.getFilename());
            book.setBookImage("/ext/bookImage/" + fileSave.getName());
            fileUpload.renameTo(fileSave);
          }
          Account account = accountService.retrieve(session(GlobalVariable.userPosition));
          book.setBookName(data.getBookName());
          book.setBookAuthor(data.getBookAuthor());
          book.setBookCost(data.getBookCost());
          book.setBookDiscount(data.getBookDiscount());
          book.setBookProduce(data.getBookProduce());
          book.setBookType(data.getBookType());
          book.setLock(data.isLock());
          book.setUpdatedAt(GlobalVariable.currentTimestamp);
          book.setUpdatedBy(account);

          bookService.edit(book);
          result = redirect(routes.BookController.editView(book.getBookId()));
        }
      } else {
        result = redirect(routes.BookController.searchView(1));
      }

      return result;
    }, ec.current());
  }

  public File fileSave(String imageName) {
    int versionFile = 1;
    File file = new File(environment.rootPath() + "/upload/bookImage/" + imageName);
    String fileTail = Convert.last(imageName.split("\\."));
    String fileName = imageName.substring(0, imageName.length() - 1 - fileTail.length());
    while (file.exists()) {
      file = new File(environment.rootPath() + "/upload/bookImage/" + fileName + "_" + versionFile + "." + fileTail);
      versionFile += 1;
    }
    return file;
  }
}



