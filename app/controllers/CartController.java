package controllers;

import actionBuilder.Authorization;
import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import forms.CartForm;
import models.entities.*;
import models.entities.Order;
import models.services.*;
import play.data.Form;
import play.data.FormFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import utils.GlobalVariable;

import javax.inject.Inject;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class CartController extends Controller {

  private HttpExecutionContext ec;
  private FormFactory formFactory;
  private CartService cartService;
  private CustomerService customerService;
  private BookService bookService;
  private OrderService orderService;
  private AccountService accountService;
  private OrderDetailService orderDetailService;

  @Inject
  public CartController(HttpExecutionContext ec, FormFactory formFactory, CartService cartService, CustomerService customerService, BookService bookService, OrderService orderService, AccountService accountService, OrderDetailService orderDetailService) {
    this.ec = ec;
    this.formFactory = formFactory;
    this.cartService = cartService;
    this.customerService = customerService;
    this.bookService = bookService;
    this.orderService = orderService;
    this.accountService = accountService;
    this.orderDetailService = orderDetailService;
  }

  @Authorization({"customer"})
  public CompletionStage<Result> index() {
    return CompletableFuture.supplyAsync(() -> {
      Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
      List<Cart> cartList = cartService.findByCustomer(customer.getCustomerId());
      int cartCount = cartService.count(customer.getCustomerId());

      return ok(views.html.cart.index.render(
        customer.getCustomerName(),
        formFactory.form(CartForm.class),
        cartList,
        cartCount
      ));
    }, ec.current());
  }

  @Authorization({"customer"})
  public CompletionStage<Result> add() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      CartForm cartForm = formFactory.form(CartForm.class).bindFromRequest().get();
      Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
      Book book = bookService.retrieve(cartForm.getBookId());

      Cart cart = cartService.retrieveByBookCustomer(customer.getCustomerId(), book.getBookId());

      if(cart == null) {
        cart = new Cart(
          customer,
          book,
          cartForm.getBookNumber()
        );

        cartService.save(cart);
        int cartNumber = cartService.count(customer.getCustomerId());
        result = ok("" + cartNumber);
      } else {
        result = badRequest();
      }
      return result;
    }, ec.current());
  }

  @Authorization({"customer"})
  public CompletionStage<Result> edit(int cartId) {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Cart cart = cartService.retrieve(cartId);

      if(cart != null) {
        CartForm cartForm = formFactory.form(CartForm.class).bindFromRequest().get();
        cart.setBookNumber(cartForm.getBookNumber());
        cartService.edit(cart);
        result = ok();
      } else {
        result = badRequest();
      }

      return result;
    }, ec.current());
  }

  @Authorization({"customer"})
  public CompletionStage<Result> delete(int cartId) {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Cart cart = cartService.retrieve(cartId);

      if(cart != null) {
        cartService.delete(cart);
        result = ok();
      } else {
        result = badRequest();
      }

      return result;
    }, ec.current());
  }

  @Authorization({"customer"})
  public CompletionStage<Result> buy() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;

      Account account = accountService.retrieve(session(GlobalVariable.userIdentity));
      Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
      List<Cart> cartList = cartService.findByCustomer(customer.getCustomerId());

      if(cartList.isEmpty()) {
        flash("error", "Giỏ hàng trống, vui lòng thêm hàng vào giỏ trước khi đặt hàng");
        result = redirect(routes.CartController.index());
      } else {
        int cost = 0;
        for(Cart cart: cartList) {
          cost += cart.getBook().getBookCost()*(100 - cart.getBook().getBookDiscount())/100 * cart.getBookNumber();
        }

        Order order = new Order(
          cost,
          customer.getCustomerAddress(),
          false,
          "pending",
          1,
          GlobalVariable.currentTimestamp,
          GlobalVariable.currentTimestamp,
          account,
          account,
          customer
        );
        orderService.save(order);

        for(Cart cart: cartList) {
          cartService.delete(cart);

          OrderDetail orderDetail = new OrderDetail(
            cart.getBookNumber(),
            order,
            cart.getBook(),
            cart.getBook().getBookCost(),
            cart.getBook().getBookDiscount()
          );

          orderDetailService.save(orderDetail);
        }

        flash("success", "Đã mua hàng thành công");
        result = redirect(routes.CartController.index());
      }

      return result;
    }, ec.current());
  }

  @Authorization({"customer"})
  public CompletionStage<Result> paypalPayment() {
    return CompletableFuture.supplyAsync(() -> {
      Result result = null;

      Account account = accountService.retrieve(session(GlobalVariable.userIdentity));
      Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
      List<Cart> cartList = cartService.findByCustomer(customer.getCustomerId());

      if(cartList.isEmpty()) {
        flash("error", "Giỏ hàng trống, vui lòng thêm hàng vào giỏ trước khi đặt hàng");
        result = redirect(routes.CartController.index());
      } else {

        String clientID = "AbgNLZpCVJcVXSlZvg9-DizpypPszJ3LvxAx5zIGl0EbRqqugGuITvhxMHQ0K7eULlWl2X0NfBMh5DjW";
        String clientSecret = "EBtVarTjsBW6aa1e6pAl9Ozpd8wZJLShx4kYI4-4AVTmWZHBY6TBqL3mCrEXzcPIzifI_Y-TQGCkPnyc";

        String executionMode = "sandbox";

        Payer payer = new Payer();
        payer.setPaymentMethod("paypal");

        // Redirect URLs
        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl("https://localhost:9000");
        redirectUrls.setReturnUrl("https://localhost:9000/cart/buy/accept_paypal?state=accept");

        // Set Payment Details Object
//        Details details = new Details();

        DecimalFormat df = new DecimalFormat("0.00");
        List<Item> items = new ArrayList<>();
        float cost = 0;
        for(Cart cart: cartList) {
          float itemCost = Float.valueOf(df.format((float)cart.getBook().getBookCost()*(100 - cart.getBook().getBookDiscount())/100 / 20000));
          System.out.println(itemCost);
          Item item = new Item(
            cart.getBook().getBookName(),
            String.valueOf(cart.getBookNumber()),
            String.valueOf(df.format(itemCost)),
            "USD"
          );
          items.add(item);
          cost += itemCost * cart.getBookNumber();
        }

        ItemList itemList = new ItemList();
        itemList.setItems(items);

        // Set Payment amount
        Amount amount = new Amount();
        amount.setCurrency("USD");
        amount.setTotal(String.valueOf(cost));
//      amount.setDetails(details);

        // Set Transaction information
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setDescription("payment paypal bookstore");
        transaction.setItemList(itemList);
        List<Transaction> transactions = new ArrayList<Transaction>();
        transactions.add(transaction);

        // Add Payment details
        Payment payment = new Payment();

        // Set Payment intent to sale
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transactions);
        payment.setRedirectUrls(redirectUrls);

        // Pass the clientID, secret and mode
        APIContext apiContext = new APIContext(clientID, clientSecret, executionMode);

        try {
          Payment createdPayment = payment.create(apiContext);
          List<Links> linkList = createdPayment.getLinks();
          for (Links link: linkList) {
            if(link.getRel().equals("approval_url")) {
              result = redirect(link.getHref());
            }
          }
        } catch (PayPalRESTException e) {
          System.out.println(e.getDetails());
        }
      }

      return result;
    }, ec.current());
  }

  @Authorization({"customer"})
  public CompletionStage<Result> acceptPaypal() {
    return CompletableFuture.supplyAsync(() -> {
      Result result = redirect(routes.Application.index());
      String state = request().getQueryString("state");
      System.out.println(request().uri());
      if(state.equals("accept")) {
        Account account = accountService.retrieve(session(GlobalVariable.userIdentity));
        Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
        List<Cart> cartList = cartService.findByCustomer(customer.getCustomerId());

        if(cartList.isEmpty()) {
          flash("error", "Giỏ hàng trống, vui lòng thêm hàng vào giỏ trước khi đặt hàng");
          result = redirect(routes.CartController.index());
        } else {
          int cost = 0;
          for(Cart cart: cartList) {
            cost += cart.getBook().getBookCost()*(100 - cart.getBook().getBookDiscount())/100 * cart.getBookNumber();
          }

          Order order = new Order(
            cost,
            customer.getCustomerAddress(),
            true,
            "pending",
            2,
            GlobalVariable.currentTimestamp,
            GlobalVariable.currentTimestamp,
            account,
            account,
            customer
          );
          orderService.save(order);

          for(Cart cart: cartList) {
            cartService.delete(cart);

            OrderDetail orderDetail = new OrderDetail(
              cart.getBookNumber(),
              order,
              cart.getBook(),
              cart.getBook().getBookCost(),
              cart.getBook().getBookDiscount()
            );

            orderDetailService.save(orderDetail);
          }

          flash("success", "Đã mua hàng thành công");
          result = redirect(routes.CartController.index());
        }
      }
      return result;
    }, ec.current());

  }

  @Authorization({"customer"})
  public CompletionStage<Result> paymentView() {
    return CompletableFuture.supplyAsync(() -> {
      return ok(views.html.payment.render());
    }, ec.current());
  }
}
