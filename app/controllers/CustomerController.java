package controllers;

import actionBuilder.Authorization;
import com.fasterxml.jackson.databind.JsonNode;
import forms.CustomerForm;
import forms.CustomerSearchForm;
import forms.StaffSearchForm;
import javafx.util.Pair;
import models.entities.Account;
import models.entities.Customer;
import models.entities.Staff;
import models.services.AccountService;
import models.services.CustomerService;
import models.services.StaffService;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import utils.GlobalVariable;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class CustomerController extends Controller {

  private HttpExecutionContext ec;
  private CustomerService customerService;
  private AccountService accountService;
  private StaffService staffService;
  private FormFactory formFactory;
  private int LIMIT = 5;

  @Inject
  public CustomerController(HttpExecutionContext ec, CustomerService customerService, AccountService accountService, StaffService staffService, FormFactory formFactory) {
    this.ec = ec;
    this.customerService = customerService;
    this.accountService = accountService;
    this.staffService = staffService;
    this.formFactory = formFactory;
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> search(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      CustomerSearchForm customerSearchData = formFactory.form(CustomerSearchForm.class).bindFromRequest().get();
      String searchSession = Json.asciiStringify(Json.toJson(customerSearchData));
      session("customerSearchForm", searchSession);

      Pair<Integer, List<Customer>> customerList = customerService.search(customerSearchData, offset, LIMIT);
      int totalPage = 0;
      if(customerList.getKey() != 0) {
        totalPage = (customerList.getKey() - 1) / LIMIT + 1;
      }

      Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
      return ok(views.html.customer.index.render(
        staff.getStaffName(),
        formFactory.form(CustomerSearchForm.class).bindFromRequest(),
        customerList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> searchView(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      String customerSearchJson = session("customerSearchForm");
      CustomerSearchForm customerSearch;

      if(customerSearchJson == null) {
        customerSearch = new CustomerSearchForm(
          "",
          ""
        );
      } else {
        JsonNode json = Json.parse(customerSearchJson);
        customerSearch = Json.fromJson(json, CustomerSearchForm.class);
      }

      Pair<Integer, List<Customer>> customerList = customerService.search(customerSearch, offset, LIMIT);
      int totalPage = 0;
      if(customerList.getKey() != 0) {
        totalPage = (customerList.getKey() - 1) / LIMIT + 1;
      }

      Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
      return ok(views.html.customer.index.render(
        staff.getStaffName(),
        formFactory.form(CustomerSearchForm.class).fill(customerSearch),
        customerList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"notSignIn"})
  public CompletionStage<Result> addView() {
    return CompletableFuture.supplyAsync(() ->
      ok(views.html.customer.add.render(formFactory.form(CustomerForm.class)))
    );
  }

  @Authorization({"notSignIn"})
  public CompletionStage<Result> add() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Form<CustomerForm> customerForm = formFactory.form(CustomerForm.class).bindFromRequest();

      if (customerForm.hasErrors()) {
        result = badRequest(views.html.customer.add.render(customerForm));
      } else {

        CustomerForm data = customerForm.get();

        Account account = new Account(
          data.getAccountName(),
          data.getAccountPassword(),
          GlobalVariable.customerPosition,
          "",
          false,
          null,
          null,
          GlobalVariable.currentTimestamp,
          GlobalVariable.currentTimestamp
        );

        Customer customer = new Customer(
          data.getCustomerName(),
          data.getCustomerAge(),
          data.getCustomerAddress(),
          data.getCustomerPhone(),
          0,
          data.getCustomerNote(),
          GlobalVariable.currentTimestamp,
          GlobalVariable.currentTimestamp,
          account
        );

        Account accountRetrieve = accountService.retrieve(account.getAccountName());

        if (accountRetrieve == null) {
          customerService.save(customer);
          result = redirect(routes.Application.index());
        } else {
          customerForm.reject("accountName", "account.equalName");
          result = badRequest(views.html.customer.add.render(customerForm));
        }

      }
      return result;
    }, ec.current());
  }

  @Authorization({"customer"})
  public CompletionStage<Result> edit(int customerId) {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Form<CustomerForm> customerForm = formFactory.form(CustomerForm.class).bindFromRequest();
      Customer customerRetrieve = customerService.retrieve(customerId);
      if(customerForm.hasErrors()) {
        result = badRequest(views.html.customer.edit.render(
          customerForm,
          customerRetrieve
        ));
      } else {
        CustomerForm data = customerForm.get();
        if(customerRetrieve == null) {
          result = redirect(routes.CustomerController.searchView(1));
        } else {
          customerRetrieve.setCustomerName(data.getCustomerName());
          customerRetrieve.setCustomerAge(data.getCustomerAge());
          customerRetrieve.setCustomerAddress(data.getCustomerAddress());
          customerRetrieve.setCustomerPhone(data.getCustomerPhone());
          customerRetrieve.setCustomerNote(data.getCustomerNote());
          customerRetrieve.setUpdatedAt(GlobalVariable.currentTimestamp);

          customerService.edit(customerRetrieve);
          result = redirect(routes.CustomerController.editView(customerId));
        }
      }
      return result;
    }, ec.current());
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> editView(int customerId) {
    return CompletableFuture.supplyAsync(() -> {
      Customer customerRetrieve = customerService.retrieve(customerId);

      CustomerForm customerForm = new CustomerForm(
        customerRetrieve.getCustomerName(),
        customerRetrieve.getCustomerAge(),
        customerRetrieve.getCustomerAddress(),
        customerRetrieve.getCustomerPhone(),
        customerRetrieve.getCustomerNote(),
        customerRetrieve.getAccount().getAccountName(),
        "******",
        "******"
      );

      return ok(views.html.customer.edit.render(
        formFactory.form(CustomerForm.class).fill(customerForm),
        customerRetrieve
      ));
    }, ec.current());
  }

}
