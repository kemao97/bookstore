package controllers;

import actionBuilder.Authorization;
import com.google.api.client.auth.oauth2.AuthorizationCodeResponseUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfoplus;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import com.restfb.scope.FacebookPermissions;
import com.restfb.scope.ScopeBuilder;
import com.restfb.types.User;
import forms.AccountForm;
import forms.PasswordForm;
import forms.PersonForm;
import models.entities.Account;
import models.entities.Customer;
import models.entities.Staff;
import models.services.AccountService;
import models.services.CustomerService;
import models.services.StaffService;
import play.data.Form;
import play.data.FormFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.libs.ws.WSClient;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utils.GlobalVariable;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class AccountController extends Controller {

  private HttpExecutionContext ec;
  private AccountService accountService;
  private FormFactory formFactory;
  private CustomerService customerService;
  private StaffService staffService;
  private WSClient wsClient;

  //Google constant
  final String clientID = "362041396622-pgqp8ukjkntvv3du11ll7sn3471s5rtr.apps.googleusercontent.com";
  final String clientSecret = "OCs2kIb29JKt6SPXwG_FaizE";

  //Facebook constant
  final String appID = "424258998352931";
  final String appSecret = "adf110a1b86aedc18600ba896b3a9baa";

  @Inject
  public AccountController(HttpExecutionContext ec, AccountService accountService, FormFactory formFactory, CustomerService customerService, StaffService staffService, WSClient wsClient) {
    this.ec = ec;
    this.accountService = accountService;
    this.formFactory = formFactory;
    this.customerService = customerService;
    this.staffService = staffService;
    this.wsClient = wsClient;
  }

  @Authorization({"notSignIn"})
  public CompletionStage<Result> signInView() {
    return CompletableFuture.supplyAsync(() ->
      ok(views.html.account.login.render(formFactory.form(AccountForm.class)))
    );
  }

  @Authorization({"notSignIn"})
  public CompletionStage<Result> signIn() {
    CompletionStage<Result> result;
    Form<AccountForm> accountForm = formFactory.form(AccountForm.class).bindFromRequest();
    if (accountForm.hasErrors()) {
      result = CompletableFuture.supplyAsync(() ->
        badRequest(views.html.account.login.render(accountForm))
      );
    } else {
      AccountForm data = accountForm.get();

      result = CompletableFuture.supplyAsync(() ->
        accountService.authenticate(data.getAccountName(), data.getAccountPassword())
      ).thenApplyAsync(accountResult -> {
        Result applyResult;
        if (accountResult == null) {
          applyResult = internalServerError(views.html.account.login.render(accountForm));
        } else if (accountResult.isLock()) {
          applyResult = notFound(views.html.account.login.render(accountForm));
        } else {
          session(GlobalVariable.userIdentity, accountResult.getAccountName());
          session(GlobalVariable.userPosition, accountResult.getAccountPosition());
          applyResult = redirect(routes.Application.index());
        }

        return applyResult;
      }, ec.current());
    }
    return result;
  }

  @Authorization({"notSignIn"})
  public CompletionStage<Result> signInFacebook() {
    CompletionStage<Result> result;

    ScopeBuilder scopeBuilder = new ScopeBuilder();
    scopeBuilder.addPermission(FacebookPermissions.EMAIL);

    DefaultFacebookClient client = new DefaultFacebookClient(Version.LATEST);
    String redirectURL = "https://localhost:9000/account/sign_in/facebook/code";

    String dialogURL = client.getLoginDialogUrl(appID, redirectURL, scopeBuilder);
    result = CompletableFuture.supplyAsync(() -> redirect(dialogURL));
    return result;
  }

  @Authorization({"notSignIn"})
  public CompletionStage<Result> signInFacebookCode() {
    return CompletableFuture.supplyAsync(() -> {
      String code = request().getQueryString("code");
      if(code != null) {
        ScopeBuilder scopeBuilder = new ScopeBuilder();
        scopeBuilder.addPermission(FacebookPermissions.EMAIL);

        DefaultFacebookClient client = new DefaultFacebookClient(Version.LATEST);
        FacebookClient.AccessToken accessToken = client.obtainUserAccessToken(appID, appSecret, "https://localhost:9000/account/sign_in/facebook/code", code);

        DefaultFacebookClient clientInfo = new DefaultFacebookClient(accessToken.getAccessToken(), Version.LATEST);
        User user = clientInfo.fetchObject("me", User.class);

        String facebookUid = user.getId();
        String fullName = user.getName();

        Account accountResult = accountService.facebookAuth(facebookUid);
        if(accountResult != null) {
          session(GlobalVariable.userIdentity, accountResult.getAccountName());
          session(GlobalVariable.userPosition, accountResult.getAccountPosition());
        } else {
          boolean flag = false;
          Account account = null;
          while (!flag) {
            account = new Account(
              GlobalVariable.getStringRandom(),
              "12345678",
              GlobalVariable.customerPosition,
              "",
              false,
              facebookUid,
              null,
              GlobalVariable.currentTimestamp,
              GlobalVariable.currentTimestamp
            );

            Customer customer = new Customer(
              fullName,
              0,
              "",
              "",
              0,
              "",
              GlobalVariable.currentTimestamp,
              GlobalVariable.currentTimestamp,
              account
            );

            Account accountRetrieve = accountService.retrieve(account.getAccountName());
            if(accountRetrieve == null) {
              customerService.save(customer);
              flag = true;
            }
          }
          session(GlobalVariable.userIdentity, account.getAccountName());
          session(GlobalVariable.userPosition, account.getAccountPosition());
        }

        return redirect(routes.Application.index());
      } else {
        return redirect(routes.Application.index());
      }
    }, ec.current());
  }

  @Authorization({"notSignIn"})
  public CompletionStage<Result> signInGoogle() {
    CompletionStage<Result> result;

    String redirectUri = "https://localhost:9000/account/sign_in/google/code";
    GoogleAuthorizationCodeRequestUrl url = new GoogleAuthorizationCodeRequestUrl(
      clientID,
      redirectUri,
      Arrays.asList(
        "https://www.googleapis.com/auth/userinfo.email",
        "https://www.googleapis.com/auth/userinfo.profile")
    );
    result = CompletableFuture.supplyAsync(() -> redirect(url.build()));

    return result;
  }

  @Authorization({"notSignIn"})
  public CompletionStage<Result> signInGoogleCode() {
    HttpTransport httpTransport = new NetHttpTransport();
    JacksonFactory jsonFactory = new JacksonFactory();

    return CompletableFuture.supplyAsync(() -> {
      Result result = ok();
      String url = "https://" + request().host() + request().uri();

      System.out.println(request().uri());

      // GET code
      AuthorizationCodeResponseUrl responseUrl = new AuthorizationCodeResponseUrl(url);

      if(responseUrl != null) {
        String code = responseUrl.getCode();

        // Use code get access token
        GoogleAuthorizationCodeTokenRequest tokenRequest = new GoogleAuthorizationCodeTokenRequest(
          httpTransport,
          jsonFactory,
          clientID,
          clientSecret,
          code,
          "https://localhost:9000/account/sign_in/google/code"
        );

        GoogleTokenResponse tokenResponse = null;
        try {
          // take token
          tokenResponse = tokenRequest.execute();

          // use token get google credential
          GoogleCredential googleCredential = new GoogleCredential.Builder()
            .setTransport(httpTransport)
            .setJsonFactory(jsonFactory)
            .setClientSecrets(clientID, clientSecret)
            .build()
            .setAccessToken(tokenResponse.getAccessToken());

          // use google credential get user info
          Oauth2 oauth2 = new Oauth2.Builder(
            httpTransport,
            jsonFactory,
            googleCredential
          ).build();

          Userinfoplus userInfo = oauth2.userinfo().get().execute();

          // create new user if account id not in database
          String googleUid = userInfo.getId();
          Account accountResult = accountService.googleAuth(googleUid);

          if(accountResult != null) {
            session(GlobalVariable.userIdentity, accountResult.getAccountName());
            session(GlobalVariable.userPosition, accountResult.getAccountPosition());
          } else {
            boolean flag = false;
            Account account = null;
            while (!flag) {
              account = new Account(
                GlobalVariable.getStringRandom(),
                "12345678",
                GlobalVariable.customerPosition,
                "",
                false,
                null,
                googleUid,
                GlobalVariable.currentTimestamp,
                GlobalVariable.currentTimestamp
              );

              Customer customer = new Customer(
                userInfo.getName(),
                0,
                "",
                "",
                0,
                "",
                GlobalVariable.currentTimestamp,
                GlobalVariable.currentTimestamp,
                account
              );

              Account accountRetrieve = accountService.retrieve(account.getAccountName());
              if(accountRetrieve == null) {
                customerService.save(customer);
                flag = true;
              }
            }
            session(GlobalVariable.userIdentity, account.getAccountName());
            session(GlobalVariable.userPosition, account.getAccountPosition());
          }
          result = redirect(routes.Application.index());
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
      return result;
    }, ec.current());
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> lockAccount(String accountName) {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Account account = accountService.retrieve(accountName);
      if (account != null) {
        account.setLock(true);
        accountService.edit(account);
        result = ok();
      } else {
        result = badRequest();
      }
      return result;
    }, ec.current());
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> unLockAccount(String accountName) {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Account account = accountService.retrieve(accountName);
      if (account != null) {
        account.setLock(false);
        accountService.edit(account);
        result = ok();
      } else {
        result = badRequest();
      }
      return result;
    }, ec.current());
  }

  @Authorization({"signIn"})
  public CompletionStage<Result> signOut() {
    CompletionStage<Result> result;

    session().remove(GlobalVariable.userIdentity);
    session().remove(GlobalVariable.userPosition);
    result = CompletableFuture.supplyAsync(() -> redirect(routes.Application.index()));

    return result;
  }

  @Authorization({"signIn"})
  public CompletionStage<Result> personalIndex() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;

      Account account = accountService.retrieve(session(GlobalVariable.userIdentity));
      Customer customer = null;
      Staff staff = null;
      PersonForm personForm;
      String fullName = "";
      if (account.getAccountPosition().equals("customer")) {
        customer = customerService.findByAccount(session(GlobalVariable.userIdentity));

        personForm = new PersonForm(
          customer.getCustomerName(),
          customer.getCustomerAge(),
          customer.getCustomerAddress(),
          customer.getCustomerPhone(),
          customer.getCustomerNote()
        );

        fullName = customer.getCustomerName();
      } else {
        staff = staffService.findByAccount(session(GlobalVariable.userIdentity));

        personForm = new PersonForm(
          staff.getStaffName(),
          staff.getStaffAge(),
          staff.getStaffAddress(),
          staff.getStaffPhone(),
          staff.getStaffNote()
        );

        fullName = staff.getStaffName();
      }

      result = ok(views.html.personal.index.render(
        fullName,
        formFactory.form(PersonForm.class).fill(personForm),
        customer,
        staff
      ));

      return result;
    }, ec.current());
  }

  @Authorization({"signIn"})
  public CompletionStage<Result> personalEdit() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;

      Staff staff = new Staff();
      Customer customer = new Customer();
      String accountName = session(GlobalVariable.userIdentity);
      String pos = session(GlobalVariable.userPosition);
      Form<PersonForm> personForm = formFactory.form(PersonForm.class).bindFromRequest();
      if (pos.equals("customer")) {
        customer = customerService.findByAccount(accountName);
        if(personForm.hasErrors()) {
          result = badRequest(views.html.personal.index.render(
            customer.getCustomerName(),
            formFactory.form(PersonForm.class).bindFromRequest(),
            customer,
            staff
          ));
        } else {
          PersonForm data = personForm.get();

          customer.setCustomerName(data.getPersonName());
          customer.setCustomerAge(data.getPersonAge());
          customer.setCustomerAddress(data.getPersonAddress());
          customer.setCustomerPhone(data.getPersonPhone());
          customer.setCustomerNote(data.getPersonNote());

          customerService.edit(customer);
          result = redirect(routes.AccountController.personalIndex());
        }
      } else {
        staff = staffService.findByAccount(accountName);
        if(personForm.hasErrors()) {
          result = badRequest(views.html.personal.index.render(
            staff.getStaffName(),
            formFactory.form(PersonForm.class).bindFromRequest(),
            customer,
            staff
          ));
        } else {
          PersonForm data = personForm.get();

          staff.setStaffName(data.getPersonName());
          staff.setStaffAge(data.getPersonAge());
          staff.setStaffAddress(data.getPersonAddress());
          staff.setStaffPhone(data.getPersonPhone());
          staff.setStaffNote(data.getPersonNote());

          staffService.edit(staff);
          result = redirect(routes.AccountController.personalIndex());
        }
      }
      return result;
    }, ec.current());
  }

  @Authorization({"signIn"})
  public CompletionStage<Result> changePasswordView() {
    return CompletableFuture.supplyAsync(() ->
      ok(views.html.personal.password.render(
        formFactory.form(PasswordForm.class)
      )), ec.current()
    );
  }

  @Authorization({"signIn"})
  public CompletionStage<Result> changePassword() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;

      Account account = accountService.retrieve(session(GlobalVariable.userIdentity));
      Form<PasswordForm> passwordForm = formFactory.form(PasswordForm.class).bindFromRequest();
      if(passwordForm.hasErrors()) {
        result = badRequest(views.html.personal.password.render(
          passwordForm
        ));
      } else {
        PasswordForm data = passwordForm.get();
        if(account.getAccountPassword().equals(data.getOldPassword())) {
          account.setAccountPassword(data.getNewPassword());

          accountService.edit(account);
          result = redirect(routes.AccountController.personalIndex());
        } else {
          passwordForm.reject("oldPassword", "Mật khẩu không chính xác");
          result = badRequest(views.html.personal.password.render(
            passwordForm
          ));
        }
      }

      return result;
    }, ec.current());
  }

  public CompletionStage<Result> loadAvatar() {
    return CompletableFuture.supplyAsync(() -> {
      Http.MultipartFormData.FilePart<Object> filePart = request().body().asMultipartFormData().getFile("personAvatarInput");
      File fileUpload = (File) filePart.getFile();

      return ok(fileUpload.getPath().replace("/var/", "/ext_var/"));
    }, ec.current());
  }
}
