package controllers;

import javafx.util.Pair;
import models.entities.Account;
import models.entities.Book;
import models.entities.Customer;
import models.entities.Staff;
import models.services.BookService;
import models.services.CartService;
import models.services.CustomerService;
import models.services.StaffService;
import play.data.FormFactory;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import utils.GlobalVariable;
import views.html.home;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Singleton
public class Application extends Controller {

  private HttpExecutionContext ec;
  private CustomerService customerService;
  private StaffService staffService;
  private BookService bookService;
  private FormFactory formFactory;
  private CartService cartService;

  private final int RECENT_RANK_LIMIT = 12;
  private final int BOOK_RANK_LIMIT = 12;

  @Inject
  public Application(HttpExecutionContext ec, CustomerService customerService, StaffService staffService, BookService bookService, FormFactory formFactory, CartService cartService) {
    this.ec = ec;
    this.customerService = customerService;
    this.staffService = staffService;
    this.bookService = bookService;
    this.formFactory = formFactory;
    this.cartService = cartService;
  }

  public CompletionStage<Result> index() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      List<Book> recentBookList = bookService.recentBook(RECENT_RANK_LIMIT);
      List<Pair<Integer, Book>> bookRank = bookService.bookRank(BOOK_RANK_LIMIT);

      String accountName = session(GlobalVariable.userIdentity);
      String position = session(GlobalVariable.userPosition);
      if(GlobalVariable.customerPosition.equals(position)) {
        Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
        int cartCount = cartService.count(customer.getCustomerId());
        result = ok(home.render(
          customer.getCustomerName(),
          cartCount,
          recentBookList,
          bookRank
        ));
      } else {
        if(accountName != null) {
          accountName = staffService.findByAccount(accountName).getStaffName();
        }
        result = ok(home.render(
          accountName,
          0,
          recentBookList,
          bookRank
        ));
      }
      return result;
    }, ec.current());
  }

}
