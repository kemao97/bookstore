package controllers;

import actionBuilder.Authorization;
import com.fasterxml.jackson.databind.JsonNode;
import forms.CustomerSearchForm;
import forms.OrderSearchForm;
import javafx.util.Pair;
import models.entities.*;
import models.services.*;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import utils.GlobalVariable;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class OrderController extends Controller {

  private HttpExecutionContext ec;
  private FormFactory formFactory;
  private OrderService orderService;
  private BookService bookService;
  private CustomerService customerService;
  private OrderDetailService orderDetailService;
  private OrderHistoryService orderHistoryService;
  private StaffService staffService;
  private AccountService accountService;

  private final int LIMIT = 5;

  @Inject
  public OrderController(HttpExecutionContext ec, FormFactory formFactory, OrderService orderService, BookService bookService, CustomerService customerService, OrderDetailService orderDetailService, AccountService accountService, OrderHistoryService orderHistoryService, StaffService staffService) {
    this.ec = ec;
    this.formFactory = formFactory;
    this.orderService = orderService;
    this.bookService = bookService;
    this.customerService = customerService;
    this.orderDetailService = orderDetailService;
    this.accountService = accountService;
    this.orderHistoryService = orderHistoryService;
    this.staffService = staffService;
  }

  @Authorization({"admin", "manager", "seller"})
  public CompletionStage<Result> search(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      OrderSearchForm orderSearchData = formFactory.form(OrderSearchForm.class).bindFromRequest().get();
      String searchSession = Json.asciiStringify(Json.toJson(orderSearchData));
      session("orderSearchForm", searchSession);

      Pair<Integer, List<OrderDetail>> orderList = orderDetailService.search(orderSearchData, offset, LIMIT);
      int totalPage = 0;
      if(orderList.getKey() != 0) {
        totalPage = (orderList.getKey() - 1) / LIMIT + 1;
      }

      Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
      return ok(views.html.order.index.render(
        staff.getStaffName(),
        formFactory.form(OrderSearchForm.class).bindFromRequest(),
        orderList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"admin", "manager", "seller"})
  public CompletionStage<Result> searchView(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      String orderSearchJson = session("orderSearchForm");
      OrderSearchForm orderSearch;

      if(orderSearchJson == null) {
        orderSearch = new OrderSearchForm(
          "",
          ""
        );
      } else {
        JsonNode json = Json.parse(orderSearchJson);
        orderSearch = Json.fromJson(json, OrderSearchForm.class);
      }

      Pair<Integer, List<OrderDetail>> orderList = orderDetailService.search(orderSearch, offset, LIMIT);
      int totalPage = 0;
      if(orderList.getKey() != 0) {
        totalPage = (orderList.getKey() - 1) / LIMIT + 1;
      }

      Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
      return ok(views.html.order.index.render(
        staff.getStaffName(),
        formFactory.form(OrderSearchForm.class).fill(orderSearch),
        orderList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"customer"})
  public CompletionStage<Result> searchMineView(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      Pair<Integer, List<OrderDetail>> orderList = orderDetailService.searchMine(session(GlobalVariable.userIdentity), offset, LIMIT);
      int totalPage = 0;
      if(orderList.getKey() != 0) {
        totalPage = (orderList.getKey() - 1) / LIMIT + 1;
      }
      System.out.println(orderList.getKey());
      System.out.println(orderList.getValue().isEmpty());

      Customer customer = customerService.findByAccount(session(GlobalVariable.userIdentity));
      return ok(views.html.personal.my_order.render(
        customer.getCustomerName(),
        orderList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"admin", "manager", "seller"})
  public CompletionStage<Result> confirm(int offset, int orderId) {
    return CompletableFuture.supplyAsync(() -> {
      Order order = orderService.retrieve(orderId);
      if(order.getOrderStatus().equals("pending") || order.getOrderStatus().equals("cancel")) {
        Account account = accountService.retrieve(session(GlobalVariable.userIdentity));
        order.setOrderStatus("delivery");
        order.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        order.setUpdatedBy(account);
        orderService.edit(order);

        Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
        OrderHistory orderHistory = new OrderHistory(
          staff,
          order,
          "confirm",
          GlobalVariable.currentTimestamp
        );
        orderHistoryService.save(orderHistory);
      }
      return redirect(routes.OrderController.search(offset));
    }, ec.current());
  }

  @Authorization({"admin", "manager", "seller"})
  public CompletionStage<Result> accept(int offset, int orderId) {
    return CompletableFuture.supplyAsync(() -> {
      Order order = orderService.retrieve(orderId);
      if(order.getOrderStatus().equals("delivery")) {
        Account account = accountService.retrieve(session(GlobalVariable.userIdentity));
        order.setOrderStatus("accept");
        order.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        order.setUpdatedBy(account);
        orderService.edit(order);

        Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
        OrderHistory orderHistory = new OrderHistory(
          staff,
          order,
          "accept",
          GlobalVariable.currentTimestamp
        );
        orderHistoryService.save(orderHistory);
      }
      return redirect(routes.OrderController.search(offset));
    }, ec.current());
  }

  @Authorization({"admin", "manager", "seller"})
  public CompletionStage<Result> cancel(int orderId) {
    return CompletableFuture.supplyAsync(() -> {
      Result result = badRequest();
      Order order = orderService.retrieve(orderId);
      if(!order.getOrderStatus().equals("accept")) {
        Account account = accountService.retrieve(session(GlobalVariable.userIdentity));
        order.setOrderStatus("cancel");
        order.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        order.setUpdatedBy(account);
        orderService.edit(order);

        Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
        OrderHistory orderHistory = new OrderHistory(
          staff,
          order,
          "cancel",
          GlobalVariable.currentTimestamp
        );
        orderHistoryService.save(orderHistory);
        result = ok();
      }
      return result;
    }, ec.current());
  }

  @Authorization({"admin", "manager", "seller"})
  public CompletionStage<Result> history(int orderId) {
    return CompletableFuture.supplyAsync(() -> {
      List<OrderHistory> orderHistoryList = orderHistoryService.findByOrder(orderId);

      return ok(views.html.order.history.render(
        orderHistoryList
      ));
    }, ec.current());
  }

}
