package controllers;

import actionBuilder.Authorization;
import com.fasterxml.jackson.databind.JsonNode;
import forms.StaffForm;
import forms.StaffSearchForm;
import javafx.util.Pair;
import models.entities.Account;
import models.entities.Staff;
import models.services.AccountService;
import models.services.StaffService;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;
import utils.GlobalVariable;

import javax.inject.Inject;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class StaffController extends Controller {

  private HttpExecutionContext ec;
  private AccountService accountService;
  private StaffService staffService;
  private FormFactory formFactory;

  private int LIMIT = 5;

  @Inject
  public StaffController(HttpExecutionContext ec, AccountService accountService, FormFactory formFactory, StaffService staffService) {
    this.ec = ec;
    this.accountService = accountService;
    this.formFactory = formFactory;
    this.staffService = staffService;
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> search(int offset) {
    return CompletableFuture.supplyAsync(() -> {
      StaffSearchForm staffSearchData = formFactory.form(StaffSearchForm.class).bindFromRequest().get();
      String searchSession = Json.asciiStringify(Json.toJson(staffSearchData));
      session("staffSearchForm", searchSession);

      Pair<Integer, List<Staff>> staffList = staffService.search(staffSearchData, offset, LIMIT);
      int totalPage = 0;
      if(staffList.getKey() != 0) {
        totalPage = (staffList.getKey() - 1) / LIMIT + 1;
      }

      Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
      return ok(views.html.staff.index.render(
        staff.getStaffName(),
        formFactory.form(StaffSearchForm.class).bindFromRequest(),
        staffList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> searchView(int offset) {

    return CompletableFuture.supplyAsync(() -> {
      String staffSearchJson = session("staffSearchForm");
      StaffSearchForm staffSearch;

      if(staffSearchJson == null) {
        staffSearch = new StaffSearchForm(
          "",
          "",
          ""
        );
      } else {
        JsonNode json = Json.parse(staffSearchJson);
        staffSearch = Json.fromJson(json, StaffSearchForm.class);
      }

      Pair<Integer, List<Staff>> staffList = staffService.search(staffSearch, offset, LIMIT);
      int totalPage = 0;
      if(staffList.getKey() != 0) {
        totalPage = (staffList.getKey() - 1) / LIMIT + 1;
      }

      Staff staff = staffService.findByAccount(session(GlobalVariable.userIdentity));
      return ok(views.html.staff.index.render(
        staff.getStaffName(),
        formFactory.form(StaffSearchForm.class).fill(staffSearch),
        staffList.getValue(),
        offset,
        totalPage
      ));
    }, ec.current());
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> add() {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Form<StaffForm> staffForm = formFactory.form(StaffForm.class).bindFromRequest();

      if (staffForm.hasErrors()) {
        result = badRequest(views.html.staff.add.render(staffForm));
      } else {

        StaffForm data = staffForm.get();
        String identityName = session(GlobalVariable.userIdentity);
        Account account = new Account(
          data.getAccountName(),
          data.getAccountPassword(),
          data.getAccountPosition(),
          "",
          false,
          null,
          null,
          GlobalVariable.currentTimestamp,
          GlobalVariable.currentTimestamp
        );

        Account createdBy = accountService.retrieve(identityName);
        Account updatedBy = createdBy;

        Staff staff = new Staff(
          data.getStaffName(),
          data.getStaffAge(),
          data.getStaffAddress(),
          data.getStaffPhone(),
          data.getStaffSalary(),
          data.getStaffNote(),
          GlobalVariable.currentTimestamp,
          GlobalVariable.currentTimestamp,
          account,
          createdBy,
          updatedBy
        );

        Account accountRetrieve = accountService.retrieve(account.getAccountName());
        if (accountRetrieve == null) {
          int staffId = staffService.save(staff);
          result = redirect(routes.StaffController.edit(staffId));
        } else {
          staffForm.reject("accountName", "account.equalName");
          result = badRequest(views.html.staff.add.render(staffForm));
        }

      }
      return result;
    }, ec.current());
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> addView() {
    return CompletableFuture.supplyAsync(() ->
      ok(views.html.staff.add.render(formFactory.form(StaffForm.class))), ec.current()
    );
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> editView(int staffId) {
    return CompletableFuture.supplyAsync(() ->{
      Staff staffRetrieve = staffService.retrieve(staffId);

      StaffForm staffForm = new StaffForm(
        staffRetrieve.getStaffName(),
        staffRetrieve.getStaffAge(),
        staffRetrieve.getStaffAddress(),
        staffRetrieve.getStaffPhone(),
        staffRetrieve.getStaffSalary(),
        staffRetrieve.getStaffNote(),
        staffRetrieve.getAccount().getAccountName(),
        "******",
        "******",
        staffRetrieve.getAccount().getAccountPosition()
      );

      return ok(views.html.staff.edit.render(
        formFactory.form(StaffForm.class).fill(staffForm),
        staffRetrieve
      ));

    }, ec.current());
  }

  @Authorization({"admin", "manager"})
  public CompletionStage<Result> edit(int staffId) {
    return CompletableFuture.supplyAsync(() -> {
      Result result;
      Form<StaffForm> staffForm = formFactory.form(StaffForm.class).bindFromRequest();
      Staff staffRetrieve = staffService.retrieve(staffId);
      if(staffForm.hasErrors()) {
        result = badRequest(views.html.staff.edit.render(
          staffForm,
          staffRetrieve
        ));
      } else {
        StaffForm data = staffForm.get();
        if(staffRetrieve == null) {
          result = redirect(routes.StaffController.searchView(1));
        } else {
          Account updatedBy = accountService.retrieve(session(GlobalVariable.userIdentity));
          Account account = staffRetrieve.getAccount();
          account.setAccountPosition(data.getAccountPosition());

          staffRetrieve.setStaffName(data.getStaffName());
          staffRetrieve.setStaffAge(data.getStaffAge());
          staffRetrieve.setStaffAddress(data.getStaffAddress());
          staffRetrieve.setStaffPhone(data.getStaffPhone());
          staffRetrieve.setStaffSalary(data.getStaffSalary());
          staffRetrieve.setStaffNote(data.getStaffNote());
          staffRetrieve.setUpdatedAt(GlobalVariable.currentTimestamp);
          staffRetrieve.setUpdatedBy(updatedBy);

          staffService.edit(staffRetrieve);
          result = redirect(routes.StaffController.editView(staffId));
        }
      }
      return result;
    }, ec.current());
  };
}
