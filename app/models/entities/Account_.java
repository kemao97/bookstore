package models.entities;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Account.class)
public abstract class Account_ {

	public static volatile SingularAttribute<Account, Timestamp> createdAt;
	public static volatile SingularAttribute<Account, String> accountName;
	public static volatile SingularAttribute<Account, String> accountPosition;
	public static volatile SingularAttribute<Account, String> accountPassword;
	public static volatile SingularAttribute<Account, String> accountAvatar;
	public static volatile SingularAttribute<Account, Boolean> isLock;
	public static volatile SingularAttribute<Account, String> facebookUid;
	public static volatile SingularAttribute<Account, String> googleUid;
	public static volatile SingularAttribute<Account, Timestamp> updatedAt;

}

