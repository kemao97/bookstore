package models.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cart.class)
public abstract class Cart_ {

  public static volatile SingularAttribute<Cart, Integer> cartId;
  public static volatile SingularAttribute<Cart, Customer> customer;
  public static volatile SingularAttribute<Cart, Book> book;
  public static volatile SingularAttribute<Cart, Integer> bookNumber;

}

