package models.entities;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Book.class)
public abstract class Book_ {

  public static volatile SingularAttribute<Book, Integer> bookId;
  public static volatile SingularAttribute<Book, String> bookName;
  public static volatile SingularAttribute<Book, String> bookType;
  public static volatile SingularAttribute<Book, Integer> bookCost;
  public static volatile SingularAttribute<Book, String> bookAuthor;
  public static volatile SingularAttribute<Book, String> bookProduce;
  public static volatile SingularAttribute<Book, Integer> bookDiscount;
  public static volatile SingularAttribute<Book, String> bookNote;
  public static volatile SingularAttribute<Book, String> bookImage;
  public static volatile SingularAttribute<Book, Boolean> isLock;
  public static volatile SingularAttribute<Book, Timestamp> createdAt;
  public static volatile SingularAttribute<Book, Timestamp> updatedAt;
  public static volatile SingularAttribute<Book, Account> createdBy;
  public static volatile SingularAttribute<Book, Account> updatedBy;

}

