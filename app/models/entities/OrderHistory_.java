package models.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Timestamp;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OrderDetail.class)
public class OrderHistory_ {

  public static volatile SingularAttribute<OrderHistory, Integer> orderHistoryId;
  public static volatile SingularAttribute<OrderHistory, String> orderHistoryStatus;
  public static volatile SingularAttribute<OrderHistory, Order> order;
  public static volatile SingularAttribute<OrderHistory, Staff> staff;
  public static volatile SingularAttribute<OrderHistory, Timestamp> createdAt;
}
