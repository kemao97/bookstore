package models.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Table(name = "customer")
public class Customer {

  @Id
  @GeneratedValue
  @Column(name = "customer_id")
  private int customerId;

  @Column(name = "customer_name")
  private String customerName;

  @Column(name = "customer_age")
  private int customerAge;

  @Column(name = "customer_address")
  private String customerAddress;

  @Column(name = "customer_phone")
  private String customerPhone;

  @Column(name = "customer_type")
  private int customerType;

  @Column(name = "customer_note")
  private String customerNote;

  @Column(name = "created_at")
  private Timestamp createdAt;

  @Column(name = "updated_at")
  private Timestamp updatedAt;

  @OneToOne
  @JoinColumn(name = "customer_acc_name")
  private Account account;

  public Customer() {}

  public Customer(String customerName, int customerAge, String customerAddress, String customerPhone, int customerType, String customerNote, Timestamp createdAt, Timestamp updatedAt, Account account) {
    this.customerName = customerName;
    this.customerAge = customerAge;
    this.customerAddress = customerAddress;
    this.customerPhone = customerPhone;
    this.customerType = customerType;
    this.customerNote = customerNote;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.account = account;
  }

  public int getCustomerId() {
    return customerId;
  }

  public void setCustomerId(int customerId) {
    this.customerId = customerId;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public int getCustomerAge() {
    return customerAge;
  }

  public void setCustomerAge(int customerAge) {
    this.customerAge = customerAge;
  }

  public String getCustomerAddress() {
    return customerAddress;
  }

  public void setCustomerAddress(String customerAddress) {
    this.customerAddress = customerAddress;
  }

  public String getCustomerPhone() {
    return customerPhone;
  }

  public void setCustomerPhone(String customerPhone) {
    this.customerPhone = customerPhone;
  }

  public int getCustomerType() {
    return customerType;
  }

  public void setCustomerType(int customerType) {
    this.customerType = customerType;
  }

  public String getCustomerNote() {
    return customerNote;
  }

  public void setCustomerNote(String customerNote) {
    this.customerNote = customerNote;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }
}
