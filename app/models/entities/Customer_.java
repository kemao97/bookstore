package models.entities;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Customer.class)
public abstract class Customer_ {

	public static volatile SingularAttribute<Customer, String> customerAddress;
	public static volatile SingularAttribute<Customer, Integer> customerAge;
	public static volatile SingularAttribute<Customer, Timestamp> createdAt;
	public static volatile SingularAttribute<Customer, String> customerPhone;
	public static volatile SingularAttribute<Customer, Integer> customerType;
	public static volatile SingularAttribute<Customer, String> customerNote;
	public static volatile SingularAttribute<Customer, Integer> customerId;
	public static volatile SingularAttribute<Customer, String> customerName;
	public static volatile SingularAttribute<Customer, Account> account;
	public static volatile SingularAttribute<Customer, Timestamp> updatedAt;

}

