package models.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "staff")
public class Staff {

  @Id
  @GeneratedValue
  @Column(name = "staff_id")
  private int staffId;

  @Column(name = "staff_name")
  private String staffName;

  @Column(name = "staff_age")
  private int staffAge;

  @Column(name = "staff_address")
  private String staffAddress;

  @Column(name = "staff_phone")
  private String staffPhone;

  @Column(name = "staff_salary")
  private int staffSalary;

  @Column(name = "staff_note")
  private String staffNote;

  @Column(name = "created_at")
  private Timestamp createdAt;

  @Column(name = "updated_at")
  private Timestamp updatedAt;

  @OneToOne
  @JoinColumn(name = "staff_acc_name")
  private Account account;

  @ManyToOne
  @JoinColumn(name = "created_by")
  private Account createdBy;

  @ManyToOne
  @JoinColumn(name = "updated_by")
  private Account updatedBy;

  public Staff() {}

  public Staff(String staffName, int staffAge, String staffAddress, String staffPhone, int staffSalary, String staffNote, Timestamp createdAt, Timestamp updatedAt, Account account, Account createdBy, Account updatedBy) {
    this.staffName = staffName;
    this.staffAge = staffAge;
    this.staffAddress = staffAddress;
    this.staffPhone = staffPhone;
    this.staffSalary = staffSalary;
    this.staffNote = staffNote;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.account = account;
    this.createdBy = createdBy;
    this.updatedBy = updatedBy;
  }

  public int getStaffId() {
    return staffId;
  }

  public void setStaffId(int staffId) {
    this.staffId = staffId;
  }

  public String getStaffName() {
    return staffName;
  }

  public void setStaffName(String staffName) {
    this.staffName = staffName;
  }

  public int getStaffAge() {
    return staffAge;
  }

  public void setStaffAge(int staffAge) {
    this.staffAge = staffAge;
  }

  public String getStaffAddress() {
    return staffAddress;
  }

  public void setStaffAddress(String staffAddress) {
    this.staffAddress = staffAddress;
  }

  public String getStaffPhone() {
    return staffPhone;
  }

  public void setStaffPhone(String staffPhone) {
    this.staffPhone = staffPhone;
  }

  public int getStaffSalary() {
    return staffSalary;
  }

  public void setStaffSalary(int staffSalary) {
    this.staffSalary = staffSalary;
  }

  public String getStaffNote() {
    return staffNote;
  }

  public void setStaffNote(String staffNote) {
    this.staffNote = staffNote;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public Account getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Account createdBy) {
    this.createdBy = createdBy;
  }

  public Account getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(Account updatedBy) {
    this.updatedBy = updatedBy;
  }
}
