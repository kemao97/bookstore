package models.entities;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Order.class)
public class Order_ {
  public static volatile SingularAttribute<Order, Integer> orderId;
  public static volatile SingularAttribute<Order, Integer> orderCost;
  public static volatile SingularAttribute<Order, String> orderAddress;
  public static volatile SingularAttribute<Order, Boolean> payed;
  public static volatile SingularAttribute<Order, String> orderStatus;
  public static volatile SingularAttribute<Order, Customer> customer;
  public static volatile SingularAttribute<Order, Timestamp> createdAt;
  public static volatile SingularAttribute<Order, Timestamp> updatedAt;
  public static volatile SingularAttribute<Order, Account> createdBy;
  public static volatile SingularAttribute<Order, Account> updatedBy;
}
