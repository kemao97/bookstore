package models.entities;

import javax.persistence.*;

@Entity
@Table(name = "cart")
public class Cart {

  @Id
  @GeneratedValue
  @Column(name = "cart_id")
  private int cardId;

  @ManyToOne
  @JoinColumn(name = "customer_id")
  private Customer customer;

  @ManyToOne
  @JoinColumn(name = "book_id")
  private Book book;

  @Column(name = "book_number")
  private int bookNumber;

  public Cart() {}

  public Cart(Customer customer, Book book, int bookNumber) {
    this.customer = customer;
    this.book = book;
    this.bookNumber = bookNumber;
  }

  public int getCardId() {
    return cardId;
  }

  public void setCardId(int cardId) {
    this.cardId = cardId;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

  public int getBookNumber() {
    return bookNumber;
  }

  public void setBookNumber(int bookNumber) {
    this.bookNumber = bookNumber;
  }
}
