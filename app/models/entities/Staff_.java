package models.entities;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Staff.class)
public abstract class Staff_ {

	public static volatile SingularAttribute<Staff, String> staffNote;
	public static volatile SingularAttribute<Staff, Timestamp> createdAt;
	public static volatile SingularAttribute<Staff, Integer> staffAge;
	public static volatile SingularAttribute<Staff, Account> updatedBy;
	public static volatile SingularAttribute<Staff, Account> createdBy;
	public static volatile SingularAttribute<Staff, String> staffName;
	public static volatile SingularAttribute<Staff, String> staffAddress;
	public static volatile SingularAttribute<Staff, String> staffPhone;
	public static volatile SingularAttribute<Staff, Integer> staffSalary;
	public static volatile SingularAttribute<Staff, Integer> staffId;
	public static volatile SingularAttribute<Staff, Account> account;
	public static volatile SingularAttribute<Staff, Timestamp> updatedAt;

}

