package models.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "account")
public class Account {

  @Id
  @Column(name = "account_name")
  private String accountName;

  @Column(name = "account_password")
  private String accountPassword;

  @Column(name = "account_position")
  private String accountPosition;

  @Column(name = "account_avatar")
  private String accountAvatar;

  @Column(name = "is_lock")
  private boolean isLock;

  @Column(name = "facebook_uid")
  private String facebookUid;

  @Column(name = "google_uid")
  private String googleUid;

  @Column(name = "created_at")
  private Timestamp createdAt;

  @Column(name = "updated_at")
  private Timestamp updatedAt;

  public Account() {

  }

  public Account(String accountName, String accountPassword, String accountPosition, String accountAvatar, boolean isLock, String facebookUid, String googleUid, Timestamp createdAt, Timestamp updatedAt) {
    this.accountName = accountName;
    this.accountPassword = accountPassword;
    this.accountPosition = accountPosition;
    this.accountAvatar = accountAvatar;
    this.isLock = isLock;
    this.facebookUid = facebookUid;
    this.googleUid = googleUid;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getAccountPassword() {
    return accountPassword;
  }

  public void setAccountPassword(String accountPassword) {
    this.accountPassword = accountPassword;
  }

  public String getAccountPosition() {
    return accountPosition;
  }

  public void setAccountPosition(String accountPosition) {
    this.accountPosition = accountPosition;
  }

  public String getAccountAvatar() {
    return accountAvatar;
  }

  public void setAccountAvatar(String accountAvatar) {
    this.accountAvatar = accountAvatar;
  }

  public boolean isLock() {
    return isLock;
  }

  public void setLock(boolean lock) {
    isLock = lock;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public String getFacebookUid() {
    return facebookUid;
  }

  public String getGoogleUid() {
    return googleUid;
  }

  public void setGoogleUid(String googleUid) {
    this.googleUid = googleUid;
  }

  public void setFacebookUid(String facebookUid) {
    this.facebookUid = facebookUid;
  }
}
