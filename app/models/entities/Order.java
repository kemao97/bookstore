package models.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "`order`")
public class Order {

  @Id
  @GeneratedValue
  @Column(name = "order_id")
  private int orderId;

  @Column(name = "order_cost")
  private int orderCost;

  @Column(name = "order_address")
  private String orderAddress;

  @Column(name = "payed")
  private Boolean payed;

  @Column(name = "order_status")
  private String orderStatus;

  @Column(name = "payment_id")
  private int paymentId;

  @Column(name = "created_at")
  private Timestamp createdAt;

  @Column(name = "updated_at")
  private Timestamp updatedAt;

  @ManyToOne
  @JoinColumn(name = "created_by")
  private Account createdBy;

  @ManyToOne
  @JoinColumn(name = "updated_by")
  private Account updatedBy;

  @ManyToOne
  @JoinColumn(name = "customer_id")
  private Customer customer;

  public Order() {}

  public Order(int orderCost, String orderAddress, Boolean payed, String orderStatus, int paymentId, Timestamp createdAt, Timestamp updatedAt, Account createdBy, Account updatedBy, Customer customer) {
    this.orderCost = orderCost;
    this.orderAddress = orderAddress;
    this.payed = payed;
    this.orderStatus = orderStatus;
    this.paymentId = paymentId;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.createdBy = createdBy;
    this.updatedBy = updatedBy;
    this.customer = customer;
  }

  public int getOrderId() {
    return orderId;
  }

  public void setOrderId(int orderId) {
    this.orderId = orderId;
  }

  public int getOrderCost() {
    return orderCost;
  }

  public void setOrderCost(int orderCost) {
    this.orderCost = orderCost;
  }

  public String getOrderAddress() {
    return orderAddress;
  }

  public void setOrderAddress(String orderAddress) {
    this.orderAddress = orderAddress;
  }

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Account getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Account createdBy) {
    this.createdBy = createdBy;
  }

  public Account getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(Account updatedBy) {
    this.updatedBy = updatedBy;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public int getPaymentId() {
    return paymentId;
  }

  public void setPaymentId(int paymentId) {
    this.paymentId = paymentId;
  }

  public Boolean getPayed() {
    return payed;
  }

  public void setPayed(Boolean payed) {
    this.payed = payed;
  }
}
