package models.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "book")
public class Book {

  @Id
  @GeneratedValue
  @Column(name = "book_id")
  private int bookId;

  @Column(name = "book_name")
  private String bookName;

  @Column(name = "book_type")
  private String bookType;

  @Column(name = "book_cost")
  private int bookCost;

  @Column(name = "book_author")
  private String bookAuthor;

  @Column(name = "book_produce")
  private String bookProduce;

  @Column(name = "book_discount")
  private int bookDiscount;

  @Column(name = "book_note")
  private String bookNote;

  @Column(name = "book_image")
  private String bookImage;

  @Column(name = "is_lock")
  private boolean isLock;

  @Column(name = "created_at")
  private Timestamp createdAt;

  @Column(name = "updated_at")
  private Timestamp updatedAt;

  @ManyToOne
  @JoinColumn(name = "created_by")
  private Account createdBy;

  @ManyToOne
  @JoinColumn(name = "updated_by")
  private Account updatedBy;

  public Book() {}

  public Book(String bookName, String bookType, int bookCost, String bookAuthor, String bookProduce, int bookDiscount, String bookNote, String bookImage, boolean isLock, Timestamp createdAt, Timestamp updatedAt, Account createdBy, Account updatedBy) {
    this.bookName = bookName;
    this.bookType = bookType;
    this.bookCost = bookCost;
    this.bookAuthor = bookAuthor;
    this.bookProduce = bookProduce;
    this.bookDiscount = bookDiscount;
    this.bookNote = bookNote;
    this.bookImage = bookImage;
    this.isLock = isLock;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.createdBy = createdBy;
    this.updatedBy = updatedBy;
  }

  public int getBookId() {
    return bookId;
  }

  public void setBookId(int bookId) {
    this.bookId = bookId;
  }

  public String getBookName() {
    return bookName;
  }

  public void setBookName(String bookName) {
    this.bookName = bookName;
  }

  public String getBookType() {
    return bookType;
  }

  public void setBookType(String bookType) {
    this.bookType = bookType;
  }

  public int getBookCost() {
    return bookCost;
  }

  public void setBookCost(int bookCost) {
    this.bookCost = bookCost;
  }

  public String getBookAuthor() {
    return bookAuthor;
  }

  public void setBookAuthor(String bookAuthor) {
    this.bookAuthor = bookAuthor;
  }

  public String getBookProduce() {
    return bookProduce;
  }

  public void setBookProduce(String bookProduce) {
    this.bookProduce = bookProduce;
  }

  public int getBookDiscount() {
    return bookDiscount;
  }

  public void setBookDiscount(int bookDiscount) {
    this.bookDiscount = bookDiscount;
  }

  public String getBookNote() {
    return bookNote;
  }

  public void setBookNote(String bookNote) {
    this.bookNote = bookNote;
  }

  public String getBookImage() {
    return bookImage;
  }

  public void setBookImage(String bookImage) {
    this.bookImage = bookImage;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }

  public Timestamp getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(Timestamp updatedAt) {
    this.updatedAt = updatedAt;
  }

  public Account getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Account createdBy) {
    this.createdBy = createdBy;
  }

  public Account getUpdatedBy() {
    return updatedBy;
  }

  public void setUpdatedBy(Account updatedBy) {
    this.updatedBy = updatedBy;
  }

  public boolean isLock() {
    return isLock;
  }

  public void setLock(boolean lock) {
    isLock = lock;
  }
}
