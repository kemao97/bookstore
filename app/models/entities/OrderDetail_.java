package models.entities;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(OrderDetail.class)
public class OrderDetail_ {
  public static volatile SingularAttribute<OrderDetail, Integer> orderDetailId;
  public static volatile SingularAttribute<OrderDetail, Integer> bookNumber;
  public static volatile SingularAttribute<OrderDetail, Order> order;
  public static volatile SingularAttribute<OrderDetail, Book> book;
  public static volatile SingularAttribute<OrderDetail, Integer> bookCost;
  public static volatile SingularAttribute<OrderDetail, Integer> bookDiscount;
  public static volatile SingularAttribute<OrderDetail, Timestamp> createdAt;
  public static volatile SingularAttribute<OrderDetail, Timestamp> updatedAt;
  public static volatile SingularAttribute<OrderDetail, Account> createdBy;
  public static volatile SingularAttribute<OrderDetail, Account> updatedBy;
}
