package models.entities;

import javax.persistence.*;

@Entity
@Table(name = "order_detail")
public class OrderDetail {

  @Id
  @GeneratedValue
  @Column(name = "order_detail_id")
  private int orderDetailId;

  @Column(name = "book_number")
  private int bookNumber;

  @ManyToOne
  @JoinColumn(name = "order_id")
  private Order order;

  @ManyToOne
  @JoinColumn(name = "book_id")
  private Book book;

  @Column(name = "book_cost")
  private int bookCost;

  @Column(name = "book_discount")
  private int bookDiscount;


  public OrderDetail() {}

  public OrderDetail(int bookNumber, Order order, Book book, int bookCost, int bookDiscount) {
    this.bookNumber = bookNumber;
    this.order = order;
    this.book = book;
    this.bookCost = bookCost;
    this.bookDiscount = bookDiscount;
  }

  public int getOrderDetailId() {
    return orderDetailId;
  }

  public void setOrderDetailId(int orderDetailId) {
    this.orderDetailId = orderDetailId;
  }

  public int getBookNumber() {
    return bookNumber;
  }

  public void setBookNumber(int bookNumber) {
    this.bookNumber = bookNumber;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

  public int getBookCost() {
    return bookCost;
  }

  public void setBookCost(int bookCost) {
    this.bookCost = bookCost;
  }

  public int getBookDiscount() {
    return bookDiscount;
  }

  public void setBookDiscount(int bookDiscount) {
    this.bookDiscount = bookDiscount;
  }
}
