package models.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "order_history")
public class OrderHistory {

  @Id
  @GeneratedValue
  @Column(name = "order_history_id")
  private int orderHistoryId;

  @ManyToOne
  @JoinColumn(name = "staff_id")
  private Staff staff;

  @ManyToOne
  @JoinColumn(name = "order_id")
  private Order order;

  @Column(name = "order_history_status")
  private String orderHistoryStatus;

  @Column(name = "created_at")
  private java.sql.Timestamp createdAt;

  public OrderHistory() {
  }

  public OrderHistory(Staff staff, Order order, String orderHistoryStatus, Timestamp createdAt) {
    this.staff = staff;
    this.order = order;
    this.orderHistoryStatus = orderHistoryStatus;
    this.createdAt = createdAt;
  }

  public int getOrderHistoryId() {
    return orderHistoryId;
  }

  public void setOrderHistoryId(int orderHistoryId) {
    this.orderHistoryId = orderHistoryId;
  }

  public Staff getStaff() {
    return staff;
  }

  public void setStaff(Staff staff) {
    this.staff = staff;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public String getOrderHistoryStatus() {
    return orderHistoryStatus;
  }

  public void setOrderHistoryStatus(String orderHistoryStatus) {
    this.orderHistoryStatus = orderHistoryStatus;
  }

  public Timestamp getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Timestamp createdAt) {
    this.createdAt = createdAt;
  }
}
