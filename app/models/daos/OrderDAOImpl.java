package models.daos;

import forms.OrderSearchForm;
import javafx.util.Pair;
import models.entities.*;
import play.db.jpa.JPAApi;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

public class OrderDAOImpl implements OrderDAO {
  private HttpExecutionContext ec;
  private JPAApi jpaApi;

  @Inject
  public OrderDAOImpl(HttpExecutionContext ec, JPAApi jpaApi) {
    this.ec = ec;
    this.jpaApi = jpaApi;
  }

  @Override
  public Order retrieve(int orderId) {
    return jpaApi.withTransaction(() ->
      jpaApi.em().find(Order.class, orderId)
    );
  }

  @Override
  public Pair<Integer, List<Order>> search(OrderSearchForm orderSearchForm, int offset, int limit) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();

      /**
       * order query
       */
      CriteriaQuery<Order> queryOrder = cb.createQuery(Order.class);
      Root<Order> fromOrder = queryOrder.from(Order.class);
      Join<Order, Customer> fromCustomer = fromOrder.join(Order_.customer);

      if(orderSearchForm.getOrderStatus().isEmpty()) {
        queryOrder.select(fromOrder).where(
          cb.like(fromCustomer.get(Customer_.customerName), "%" + orderSearchForm.getCustomerName() + "%")
        );
      } else {
        queryOrder.select(fromOrder).where(
          cb.like(fromCustomer.get(Customer_.customerName), "%" + orderSearchForm.getCustomerName() + "%"),
          cb.equal(fromOrder.get(Order_.orderStatus), orderSearchForm.getOrderStatus())
        );
      }

      /**
       * count query
       */
      CriteriaQuery<Long> queryCount = cb.createQuery(Long.class);
      fromOrder = queryCount.from(Order.class);
      fromCustomer = fromOrder.join(Order_.customer);

      if(orderSearchForm.getOrderStatus().isEmpty()) {
        queryCount.select(cb.count(fromOrder)).where(
          cb.like(fromCustomer.get(Customer_.customerName), "%" + orderSearchForm.getCustomerName() + "%")
        );
      } else {
        queryCount.select(cb.count(fromOrder)).where(
          cb.like(fromCustomer.get(Customer_.customerName), "%" + orderSearchForm.getCustomerName() + "%"),
          cb.equal(fromOrder.get(Order_.orderStatus), orderSearchForm.getOrderStatus())
        );
      }

      /**
       * result
       */
      return new Pair<>(
        em.createQuery(queryCount).getSingleResult().intValue(),
        em.createQuery(queryOrder).setFirstResult((offset - 1) * limit).setMaxResults(limit).getResultList()
      );
    });
  }

  @Override
  public Pair<Integer, List<Order>> searchMine(String username, int offset, int limit) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();

      /**
       * order query
       */
      CriteriaQuery<Order> queryOrder = cb.createQuery(Order.class);
      Root<Order> fromOrder = queryOrder.from(Order.class);
      Join<Order, Customer> fromCustomer = fromOrder.join(Order_.customer);
      Join<Customer, Account> fromAccount = fromCustomer.join(Customer_.account);

      cb.equal(fromAccount.get(Account_.accountName), username);

      /**
       * count query
       */
      CriteriaQuery<Long> queryCount = cb.createQuery(Long.class);
      fromOrder = queryCount.from(Order.class);
      fromCustomer = fromOrder.join(Order_.customer);
      fromAccount = fromCustomer.join(Customer_.account);

      cb.equal(fromAccount.get(Account_.accountName), username);

      /**
       * result
       */
      return new Pair<>(
        em.createQuery(queryCount).getSingleResult().intValue(),
        em.createQuery(queryOrder).setFirstResult((offset - 1) * limit).setMaxResults(limit).getResultList()
      );
    });
  }

  @Override
  public int save(Order order) {
    return jpaApi.withTransaction(() -> {
      jpaApi.em().persist(order);
      return order.getOrderId();
    });
  }

  @Override
  public void edit(Order order) {
    jpaApi.withTransaction(() ->
      jpaApi.em().merge(order)
    );
  }
}
