package models.daos;

import models.entities.Cart;

import java.util.List;

public interface CartDAO {

  Cart retrieve(int cartId);

  int count(int customerId);

  Cart retrieveByBookCustomer(int customerId, int bookId);

  List<Cart> findByCustomer(int customerId);

  int save(Cart cart);

  void edit(Cart cart);

  void delete(Cart cart);

}
