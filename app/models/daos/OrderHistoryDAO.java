package models.daos;

import models.entities.OrderHistory;

import java.util.List;

public interface OrderHistoryDAO {

  int save(OrderHistory orderHistory);

  List<OrderHistory> findByStaff(int staffId);

  List<OrderHistory> findByOrder(int orderId);

}
