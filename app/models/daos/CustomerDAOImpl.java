package models.daos;

import forms.CustomerSearchForm;
import javafx.util.Pair;
import models.entities.Account;
import models.entities.Account_;
import models.entities.Customer;
import models.entities.Customer_;
import play.db.jpa.JPAApi;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO {
  private HttpExecutionContext ec;
  private JPAApi jpaApi;

  @Inject
  public CustomerDAOImpl(HttpExecutionContext ec, JPAApi jpaApi) {
    this.ec = ec;
    this.jpaApi = jpaApi;
  }

  @Override
  public Customer retrieve(int customerId) {
    return jpaApi.withTransaction(() ->
      jpaApi.em().find(Customer.class, customerId)
    );
  }

  @Override
  public Customer findByAccount(String accountName) {
    return jpaApi.withTransaction(() -> {
      Customer customer = null;
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();
      CriteriaQuery<Customer> query = cb.createQuery(Customer.class);
      Join<Customer, Account> fromAccount = query.from(Customer.class).join(Customer_.account);

      query.select(query.getSelection()).where(
        cb.equal(fromAccount.get(Account_.accountName), accountName)
      );

      try {
        customer = em.createQuery(query).getSingleResult();
      } catch (NoResultException ex) {

      }

      return customer;
    });
  }

  @Override
  public Pair<Integer, List<Customer>> search(CustomerSearchForm customerSearch, int offset, int limit) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();

      /**
       * customer query
       */
      CriteriaQuery<Customer> queryCustomer = cb.createQuery(Customer.class);
      Root<Customer> fromCustomer = queryCustomer.from(Customer.class);
      Join<Customer, Account> fromAccount = fromCustomer.join(Customer_.account);

      queryCustomer.select(fromCustomer).where(
        cb.like(fromAccount.get(Account_.accountName), "%" + customerSearch.getAccountName() + "%"),
        cb.like(fromCustomer.get(Customer_.customerName), "%" + customerSearch.getCustomerName() + "%")
      );

      /**
       * count query
       */
      CriteriaQuery<Long> queryCount = cb.createQuery(Long.class);
      fromCustomer = queryCount.from(Customer.class);
      fromAccount = fromCustomer.join(Customer_.account);

      queryCount.select(cb.count(fromCustomer)).where(
        cb.like(fromAccount.get(Account_.accountName), "%" + customerSearch.getAccountName() + "%"),
        cb.like(fromCustomer.get(Customer_.customerName), "%" + customerSearch.getCustomerName() + "%")
      );

      /**
       * result
       */
      return new Pair<>(
        em.createQuery(queryCount).getSingleResult().intValue(),
        em.createQuery(queryCustomer).setFirstResult((offset - 1) * limit).setMaxResults(limit).getResultList()
      );
    });
  }

  @Override
  public List<Customer> all() {

    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();
      CriteriaQuery<Customer> customer = cb.createQuery(Customer.class);
      return em.createQuery(customer.select(customer.getSelection())).getResultList();
    });
  }

  @Override
  public int save(Customer customer) {

    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      em.persist(customer.getAccount());
      em.persist(customer);
      return customer.getCustomerId();
    });
  }

  @Override
  public int edit(Customer customer) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      em.merge(customer.getAccount());
      em.merge(customer);
      return customer.getCustomerId();
    });
  }
}
