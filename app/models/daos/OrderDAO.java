package models.daos;

import forms.OrderSearchForm;
import javafx.util.Pair;
import models.entities.Order;

import java.util.List;

public interface OrderDAO {

  Order retrieve(int orderId);

  Pair<Integer, List<Order>> search(OrderSearchForm orderSearchForm, int offset, int limit);

  Pair<Integer, List<Order>> searchMine(String username, int offset, int limit);

  int save(Order order);

  void edit(Order order);

}
