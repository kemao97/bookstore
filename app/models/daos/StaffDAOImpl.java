package models.daos;

import forms.StaffForm;
import forms.StaffSearchForm;
import javafx.util.Pair;
import models.entities.Account;
import models.entities.Account_;
import models.entities.Staff;
import models.entities.Staff_;
import play.data.Form;
import play.db.jpa.JPAApi;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

public class StaffDAOImpl implements StaffDAO {

  private HttpExecutionContext ec;
  private JPAApi jpaApi;

  @Inject
  public StaffDAOImpl(HttpExecutionContext ec, JPAApi jpaApi) {
    this.ec = ec;
    this.jpaApi = jpaApi;
  }

  @Override
  public Staff retrieve(int staffId) {
    return jpaApi.withTransaction(() ->
      jpaApi.em().find(Staff.class, staffId)
    );
  }

  @Override
  public Staff findByAccount(String accountName) {
    return jpaApi.withTransaction(() -> {
      Query queryStaff = jpaApi.em().createQuery(
        "SELECT s" +
          " FROM Staff s" +
          " WHERE s.account.accountName = :accountName"
      );
      queryStaff.setParameter("accountName", accountName);
      return (Staff) queryStaff.getSingleResult();
    });
  }

  @Override
  public Pair<Integer, List<Staff>> search(StaffSearchForm staffSearch, int offset, int limit) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();

      /**
       * staff query
       */
      CriteriaQuery<Staff> queryStaff = cb.createQuery(Staff.class);
      Root<Staff> fromStaff = queryStaff.from(Staff.class);
      Join<Staff, Account> fromAccount = fromStaff.join(Staff_.account);

      if(staffSearch.getPosition().isEmpty()) {
        queryStaff.select(fromStaff).where(
          cb.like(fromAccount.get(Account_.accountName), "%" + staffSearch.getAccountName() + "%"),
          cb.like(fromStaff.get(Staff_.staffName), "%" + staffSearch.getStaffName() + "%"),
          cb.notEqual(fromAccount.get(Account_.accountName), "admin")
        );
      } else {
        queryStaff.select(fromStaff).where(
          cb.like(fromAccount.get(Account_.accountName), "%" + staffSearch.getAccountName() + "%"),
          cb.like(fromStaff.get(Staff_.staffName), "%" + staffSearch.getStaffName() + "%"),
          cb.equal(fromAccount.get(Account_.accountPosition), staffSearch.getPosition()),
          cb.notEqual(fromAccount.get(Account_.accountName), "admin")
        );
      }

      /**
       * count query
       */
      CriteriaQuery<Long> queryCount = cb.createQuery(Long.class);
      fromStaff = queryCount.from(Staff.class);
      fromAccount = fromStaff.join(Staff_.account);

      if(staffSearch.getPosition().isEmpty()) {
        queryCount.select(cb.count(fromStaff)).where(
          cb.like(fromAccount.get(Account_.accountName), "%" + staffSearch.getAccountName() + "%"),
          cb.like(fromStaff.get(Staff_.staffName), "%" + staffSearch.getStaffName() + "%"),
          cb.notEqual(fromAccount.get(Account_.accountName), "admin")
        );
      } else {
        queryCount.select(cb.count(fromStaff)).where(
          cb.like(fromAccount.get(Account_.accountName), "%" + staffSearch.getAccountName() + "%"),
          cb.like(fromStaff.get(Staff_.staffName), "%" + staffSearch.getStaffName() + "%"),
          cb.equal(fromAccount.get(Account_.accountPosition), staffSearch.getPosition()),
          cb.notEqual(fromAccount.get(Account_.accountName), "admin")
        );
      }

      /**
       * result
       */
      return new Pair<>(
        em.createQuery(queryCount).getSingleResult().intValue(),
        em.createQuery(queryStaff).setFirstResult((offset - 1) * limit).setMaxResults(limit).getResultList()
      );
    });
  }

  @Override
  public int save(Staff staff) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      em.persist(staff.getAccount());
      em.persist(staff);
      return staff.getStaffId();
    });
  }

  @Override
  public int edit(Staff staff) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      em.merge(staff.getAccount());
      Staff staffResult = em.merge(staff);
      return staffResult.getStaffId();
    });
  }
}
