package models.daos;

import forms.OrderSearchForm;
import javafx.util.Pair;
import models.entities.*;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.List;

public class OrderDetailDAOImpl implements OrderDetailDAO {

  private JPAApi jpaApi;

  @Inject
  public OrderDetailDAOImpl(JPAApi jpaApi) {
    this.jpaApi = jpaApi;
  }

  @Override
  public OrderDetail retrieve(int orderDetailId) {
    return jpaApi.withTransaction(() ->
      jpaApi.em().find(OrderDetail.class, orderDetailId)
    );
  }

  @Override
  public Pair<Integer, List<OrderDetail>> search(OrderSearchForm orderSearchForm, int offset, int limit) {
    return jpaApi.withTransaction(() -> {
      /**
       * order query
       */
      String queryWhere = " c.customer_name LIKE :customerName";
      if(!orderSearchForm.getOrderStatus().isEmpty()) {
        queryWhere += " AND o.order_status = :orderStatus";
      }

      String queryOrder =
        " (SELECT DISTINCT o.order_id, o.customer_id, o.order_status" +
        " FROM `order` o";
      if(!orderSearchForm.getOrderStatus().isEmpty()) {
        queryOrder += " WHERE o.order_status = :orderStatus";
      }
      queryOrder += " LIMIT :start, :limit) o";
      Query queryOrderDetail = jpaApi.em().createNativeQuery(
          " SELECT os.order_id, os.book_cost, os.book_discount, os.book_id, os.book_number, os.order_detail_id" +
          " FROM " + queryOrder +
          " JOIN order_detail AS os" +
          " ON o.order_id = os.order_id" +
          " JOIN customer AS c" +
          " ON o.customer_id = c.customer_id" +
          " WHERE" + queryWhere
      , OrderDetail.class);

      /**
       * count query
       */
      Query queryCount = jpaApi.em().createNativeQuery(
        "SELECT DISTINCT COUNT(*)" +
          " FROM `order` o" +
          " JOIN customer c" +
          " ON o.customer_id = c.customer_id" +
          " WHERE" + queryWhere
      );
      /**
       * setParameter queryOrderDetail
       */
      queryOrderDetail.setParameter("start", (offset - 1) * limit);
      queryOrderDetail.setParameter("limit", limit);
      queryOrderDetail.setParameter("customerName", "%" + orderSearchForm.getCustomerName() + "%");

      /**
       * setParameter queryCount
       */
      queryCount.setParameter("customerName", "%" + orderSearchForm.getCustomerName() + "%");

      /**
       * setParameter selectbox
       */
      if(!orderSearchForm.getOrderStatus().isEmpty()) {
        queryOrderDetail.setParameter("orderStatus", orderSearchForm.getOrderStatus());
        queryCount.setParameter("orderStatus", orderSearchForm.getOrderStatus());
      }

      /**
       * result
       */
      return new Pair<>(
        ((BigInteger)queryCount.getSingleResult()).intValue(),
        queryOrderDetail.getResultList()
      );
    });
  }

  @Override
  public Pair<Integer, List<OrderDetail>> searchMine(String username, int offset, int limit) {
    return jpaApi.withTransaction(() -> {
      /**
       * order query
       */
      String queryWhere = " c.customer_acc_name = :username";

      String queryOrder =
        " (SELECT DISTINCT o.order_id, o.customer_id, o.order_status" +
          " FROM `order` o" +
          " JOIN customer AS c" +
          " ON o.customer_id = c.customer_id" +
          " WHERE" + queryWhere;
      queryOrder += " LIMIT :start, :limit) o";
      Query queryOrderDetail = jpaApi.em().createNativeQuery(
        " SELECT os.order_id, os.book_cost, os.book_discount, os.book_id, os.book_number, os.order_detail_id" +
          " FROM " + queryOrder +
          " JOIN order_detail AS os" +
          " ON o.order_id = os.order_id"
        , OrderDetail.class);

      /**
       * count query
       */
      Query queryCount = jpaApi.em().createNativeQuery(
        "SELECT DISTINCT COUNT(*)" +
          " FROM `order` o" +
          " JOIN customer c" +
          " ON o.customer_id = c.customer_id" +
          " WHERE" + queryWhere
      );
      /**
       * setParameter queryOrderDetail
       */
      queryOrderDetail.setParameter("start", (offset - 1) * limit);
      queryOrderDetail.setParameter("limit", limit);
      queryOrderDetail.setParameter("username", username);

      /**
       * setParameter queryCount
       */
      queryCount.setParameter("username", username);

      /**
       * result
       */
      return new Pair<>(
        ((BigInteger)queryCount.getSingleResult()).intValue(),
        queryOrderDetail.getResultList()
      );
    });
  }

  @Override
  public int save(OrderDetail orderDetail) {
    return jpaApi.withTransaction(() -> {
      jpaApi.em().persist(orderDetail);
      return orderDetail.getOrderDetailId();
    });
  }

  @Override
  public void edit(OrderDetail orderDetail) {
    jpaApi.withTransaction(() ->
      jpaApi.em().merge(orderDetail)
    );
  }


}
