package models.daos;

import forms.BookSearchForm;
import javafx.util.Pair;
import models.entities.Book;
import models.entities.Book_;
import models.entities.OrderDetail;
import models.entities.OrderDetail_;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class BookDAOImpl implements BookDAO {

  private JPAApi jpaApi;

  @Inject
  public BookDAOImpl(JPAApi jpaApi) {
    this.jpaApi = jpaApi;
  }

  @Override
  public Book retrieve(int bookId) {
    return jpaApi.withTransaction(() ->
      jpaApi.em().find(Book.class, bookId)
    );
  }

  @Override
  public List<Book> recentBook(int limit) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();

      CriteriaQuery<Book> queryBook = cb.createQuery(Book.class);
      Root<Book> fromBook = queryBook.from(Book.class);

      queryBook.select(fromBook).where(cb.equal(fromBook.get(Book_.isLock), new Boolean(false)))
        .orderBy(cb.desc(fromBook.get(Book_.createdAt)));

      return em.createQuery(queryBook).setMaxResults(limit).getResultList();
    });
  }

  @Override
  public List<Pair<Integer, Book>> bookRank(int limit) {
    return jpaApi.withTransaction(() -> {
      List<Object[]> book = jpaApi.em().createQuery(
        "SELECT SUM(o.bookNumber), b" +
          " FROM Book b, OrderDetail o, Order os" +
          " WHERE b.bookId = o.book.bookId AND o.order.orderId = os.orderId AND os.orderStatus = 'accept' AND b.isLock = false" +
          " GROUP BY b" +
          " ORDER BY SUM(o.bookNumber) DESC"
      ).setMaxResults(limit).getResultList();
      List<Pair<Integer, Book>> result = new ArrayList<>();
      for(Object[] bookAndNumber: book) {
        Pair<Integer, Book> pair = new Pair<>(((Long) bookAndNumber[0]).intValue(), (Book) bookAndNumber[1]);
        result.add(pair);
      }
      return result;
    });
  }

  @Override
  public Pair<Integer, List<Book>> search(BookSearchForm bookSearch, int offset, int limit) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();

      /**
       * book query
       */
      CriteriaQuery<Book> queryBook = cb.createQuery(Book.class);
      Root<Book> fromBook = queryBook.from(Book.class);

      if(bookSearch.getBookType().isEmpty()) {
        queryBook.select(fromBook).where(
          cb.like(fromBook.get(Book_.bookName), "%" + bookSearch.getBookName() + "%")
        );
      } else {
        queryBook.select(fromBook).where(
          cb.like(fromBook.get(Book_.bookName), "%" + bookSearch.getBookName() + "%"),
          cb.equal(fromBook.get(Book_.bookType), bookSearch.getBookType())
        );
      }

      /**
       * count query
       */
      CriteriaQuery<Long> queryCount = cb.createQuery(Long.class);
      fromBook = queryCount.from(Book.class);

      if(bookSearch.getBookType().isEmpty()) {
        queryCount.select(cb.count(fromBook)).where(
          cb.like(fromBook.get(Book_.bookName), "%" + bookSearch.getBookName() + "%")
        );
      } else {
        queryCount.select(cb.count(fromBook)).where(
          cb.like(fromBook.get(Book_.bookName), "%" + bookSearch.getBookName() + "%"),
          cb.equal(fromBook.get(Book_.bookType), bookSearch.getBookType())
        );
      }

      /**
       * result
       */
      return new Pair<>(
        em.createQuery(queryCount).getSingleResult().intValue(),
        em.createQuery(queryBook).setFirstResult((offset - 1) * limit).setMaxResults(limit).getResultList()
      );
    });
  }

  @Override
  public Pair<Integer, List<Book>> bookSearch(BookSearchForm bookSearch, int offset, int limit) {
    return jpaApi.withTransaction(() -> {
      String bookTypeQueryCat = "";
      if(!bookSearch.getBookType().isEmpty()) {
        bookTypeQueryCat = " AND b.bookType = :bookType";
      }

      Query countQuery = jpaApi.em().createQuery(
        "SELECT COUNT(*)" +
          " FROM Book b" +
          " WHERE b.bookName LIKE :bookNameLike" + bookTypeQueryCat +
          " AND b.isLock = false"
      );
      countQuery.setParameter("bookNameLike", '%' + bookSearch.getBookName() + '%');
      if(!bookSearch.getBookType().isEmpty()) {
        countQuery.setParameter("bookType", bookSearch.getBookType());
      }

      Query bookQuery = jpaApi.em().createQuery(
        "SELECT b" +
          " FROM Book b" +
          " WHERE b.bookName LIKE :bookNameLike" + bookTypeQueryCat +
          " AND b.isLock = false"
      );
      bookQuery.setParameter("bookNameLike", '%' + bookSearch.getBookName() + '%');
      if(!bookSearch.getBookType().isEmpty()) {
        bookQuery.setParameter("bookType", bookSearch.getBookType());
      }
      return new Pair<>(
        ((Long)countQuery.getSingleResult()).intValue(),
        bookQuery.setFirstResult((offset - 1) * limit).setMaxResults(limit).getResultList()
      );
    });
  }

  @Override
  public int save(Book book) {
    return jpaApi.withTransaction(() -> {
      jpaApi.em().persist(book);
      return book.getBookId();
    });
  }

  @Override
  public void edit(Book book) {
    jpaApi.withTransaction(() -> {
      jpaApi.em().merge(book);
    });
  }
}
