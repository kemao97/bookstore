package models.daos;

import models.entities.Cart;
import models.entities.Cart_;
import models.entities.Customer;
import models.entities.Customer_;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

public class CartDAOImpl implements CartDAO {

  private JPAApi jpaApi;

  @Inject
  public CartDAOImpl(JPAApi jpaApi) {
    this.jpaApi = jpaApi;
  }

  @Override
  public Cart retrieve(int cartId) {
    return jpaApi.withTransaction(() ->
      jpaApi.em().find(Cart.class, cartId)
    );
  }

  @Override
  public int count(int customerId) {
    return jpaApi.withTransaction(() -> {
      Query queryCount = jpaApi.em().createQuery(
        "SELECT COUNT(*)" +
          " FROM Customer c, Cart ca" +
          " WHERE c.customerId = ca.customer.customerId" +
          " AND c.customerId = :customerId"
      );
      queryCount.setParameter("customerId", customerId);

      return ((Long)queryCount.getSingleResult()).intValue();
    });
  }

  @Override
  public Cart retrieveByBookCustomer(int customerId, int bookId) {
    return jpaApi.withTransaction(() -> {
      Cart cart = null;
      try {
        cart = (Cart) jpaApi.em().createQuery(
          "SELECT c FROM Cart c WHERE c.book.bookId = " + bookId + " AND c.customer.customerId = " + customerId
        ).getSingleResult();
      } catch (NoResultException ex) {

      }
      return cart;
    });
  }

  @Override
  public List<Cart> findByCustomer(int customerId) {
    return jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      CriteriaBuilder cb = em.getCriteriaBuilder();
      CriteriaQuery<Cart> query = cb.createQuery(Cart.class);
      Join<Cart, Customer> fromCustomer = query.from(Cart.class).join(Cart_.customer);

      query.select(query.getSelection()).where(
        cb.equal(fromCustomer.get(Customer_.customerId), customerId)
      );

      return em.createQuery(query).getResultList();
    });
  }

  @Override
  public int save(Cart cart) {
    return jpaApi.withTransaction(() -> {
      jpaApi.em().persist(cart);
      return cart.getCardId();
    });
  }

  @Override
  public void edit(Cart cart) {
    jpaApi.withTransaction(() -> {
      jpaApi.em().merge(cart);
    });
  }

  @Override
  public void delete(Cart cart) {
    jpaApi.withTransaction(() -> {
      EntityManager em = jpaApi.em();
      em.remove(em.contains(cart) ? cart : em.merge(cart));
    });
  }
}
