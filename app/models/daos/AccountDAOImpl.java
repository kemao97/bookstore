package models.daos;

import models.entities.Account;
import play.db.jpa.JPAApi;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class AccountDAOImpl implements AccountDAO {

  private HttpExecutionContext ec;
  private JPAApi jpaApi;

  @Inject
  public AccountDAOImpl(HttpExecutionContext ec, JPAApi jpaApi) {
    this.ec = ec;
    this.jpaApi = jpaApi;
  }

  @Override
  public Account retrieve(String accountName) {
    return jpaApi.withTransaction(() ->
      jpaApi.em().find(Account.class, accountName)
    );
  }

  @Override
  public String save(Account account) {
    return jpaApi.withTransaction(() -> {
      jpaApi.em().persist(account);
      return account.getAccountName();
    });
  }

  @Override
  public void edit(Account account) {
    jpaApi.withTransaction(() ->
      jpaApi.em().merge(account)
    );
  }

  @Override
  public Account authenticate(String username, String password) {
    return jpaApi.withTransaction(() -> {
      Account result = null;
      Account accountFind = retrieve(username);
      if(accountFind == null) {
        result = null;
      } else if(accountFind.getAccountPassword().equals(password)) {
        result = accountFind;
      }
      return result;
    });
  }

  @Override
  public Account facebookAuth(String facebookUid) {
    return jpaApi.withTransaction(() -> {
      Account account = null;
      Query queryCount = jpaApi.em().createQuery(
        "SELECT a" +
          " FROM Account a" +
          " WHERE a.facebookUid = :facebookUid"
      );
      queryCount.setParameter("facebookUid", facebookUid);

      try {
        account = (Account) queryCount.getSingleResult();
      } catch (NoResultException ex) {
        ex.printStackTrace();
      }
      return account;
    });
  }

  @Override
  public Account googleAuth(String googleUid) {
    return jpaApi.withTransaction(() -> {
      Account account = null;
      Query queryCount = jpaApi.em().createQuery(
        "SELECT a" +
          " FROM Account a" +
          " WHERE a.googleUid = :googleUid"
      );
      queryCount.setParameter("googleUid", googleUid);

      try {
        account = (Account) queryCount.getSingleResult();
      } catch (NoResultException ex) {
        ex.printStackTrace();
      }
      return account;
    });
  }
}
