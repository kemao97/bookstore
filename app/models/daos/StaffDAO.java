package models.daos;

import forms.StaffSearchForm;
import javafx.util.Pair;
import models.entities.Staff;
import play.data.Form;

import java.util.List;

public interface StaffDAO {

  Staff retrieve(int staffId);

  Pair<Integer, List<Staff>> search(StaffSearchForm staffSearch, int offset, int limit);

  Staff findByAccount(String accountName);

  int save(Staff staff);

  int edit(Staff staff);

}
