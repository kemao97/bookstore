package models.daos;

import models.entities.OrderHistory;
import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.Query;
import java.util.List;

public class OrderHistoryDAOImpl implements OrderHistoryDAO {

  private JPAApi jpaApi;

  @Inject
  public OrderHistoryDAOImpl(JPAApi jpaApi) {
    this.jpaApi = jpaApi;
  }

  @Override
  public int save(OrderHistory orderHistory) {
    return jpaApi.withTransaction(() -> {
      jpaApi.em().persist(orderHistory);
      return orderHistory.getOrderHistoryId();
    });
  }

  @Override
  public List<OrderHistory> findByStaff(int staffId) {
    return jpaApi.withTransaction(() -> {
      Query query = jpaApi.em().createQuery(
        "SELECT o" +
          " FROM OrderHistory o" +
          " WHERE o.staff.staffId = :staffId"
      );
      query.setParameter("staffId", staffId);
      return query.getResultList();
    });
  }

  @Override
  public List<OrderHistory> findByOrder(int orderId) {
    return jpaApi.withTransaction(() -> {
      Query query = jpaApi.em().createQuery(
        "SELECT o" +
          " FROM OrderHistory o" +
          " WHERE o.order.orderId = :orderId"
      );
      query.setParameter("orderId", orderId);
      return query.getResultList();
    });
  }
}
