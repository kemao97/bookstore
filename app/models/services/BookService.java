package models.services;

import forms.BookSearchForm;
import javafx.util.Pair;
import models.entities.Book;

import java.util.List;

public interface BookService {

  Book retrieve(int bookId);

  List<Book> recentBook(int limit);

  List<Pair<Integer, Book>> bookRank(int limit);

  Pair<Integer, List<Book>> search(BookSearchForm bookSearch, int offset, int limit);

  Pair<Integer, List<Book>> bookSearch(BookSearchForm bookSearch, int offset, int limit);

  int save(Book book);

  void edit(Book book);

}
