package models.services;

import forms.OrderSearchForm;
import javafx.util.Pair;
import models.entities.OrderDetail;

import java.util.List;

public interface OrderDetailService {

  OrderDetail retrieve(int orderDetailId);

  Pair<Integer, List<OrderDetail>> search(OrderSearchForm orderSearchForm, int offset, int limit);

  Pair<Integer, List<OrderDetail>> searchMine(String username, int offset, int limit);

  int save(OrderDetail orderDetail);

  void edit(OrderDetail orderDetail);

}
