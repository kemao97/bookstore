package models.services;

import forms.CustomerSearchForm;
import javafx.util.Pair;
import models.entities.Customer;

import java.util.List;

public interface CustomerService {

  Customer retrieve(int customerId);

  Customer findByAccount(String accountName);

  Pair<Integer, List<Customer>> search(CustomerSearchForm customerSearch, int offset, int limit);

  List<Customer> all();

  int save(Customer customer);

  int edit(Customer customer);
}
