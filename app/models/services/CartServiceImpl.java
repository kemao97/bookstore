package models.services;

import models.daos.CartDAO;
import models.entities.Cart;

import javax.inject.Inject;
import java.util.List;

public class CartServiceImpl implements CartService {

  private CartDAO cartDAO;

  @Inject
  public CartServiceImpl(CartDAO cartDAO) {
    this.cartDAO = cartDAO;
  }

  @Override
  public Cart retrieve(int cartId) {
    return cartDAO.retrieve(cartId);
  }

  @Override
  public int count(int customerId) {
    return cartDAO.count(customerId);
  }

  @Override
  public Cart retrieveByBookCustomer(int customerId, int bookId) {
    return cartDAO.retrieveByBookCustomer(customerId, bookId);
  }

  @Override
  public List<Cart> findByCustomer(int customerId) {
    return cartDAO.findByCustomer(customerId);
  }

  @Override
  public int save(Cart cart) {
    return cartDAO.save(cart);
  }

  @Override
  public void edit(Cart cart) {
    cartDAO.edit(cart);
  }

  @Override
  public void delete(Cart cart) {
    cartDAO.delete(cart);
  }
}
