package models.services;

import forms.StaffSearchForm;
import javafx.util.Pair;
import models.daos.StaffDAO;
import models.entities.Staff;
import play.data.Form;

import javax.inject.Inject;
import java.util.List;

public class StaffServiceImpl implements StaffService {

  @Inject StaffDAO staffDAO;

  @Override
  public Staff retrieve(int staffId) {
    return staffDAO.retrieve(staffId);
  }

  @Override
  public Pair<Integer, List<Staff>> search(StaffSearchForm staffSearch, int offset, int limit) {
    return staffDAO.search(staffSearch, offset, limit);
  }

  @Override
  public Staff findByAccount(String accountName) {
    return staffDAO.findByAccount(accountName);
  }

  @Override
  public int save(Staff staff) {
    return staffDAO.save(staff);
  }

  @Override
  public int edit(Staff staff) {
    return staffDAO.edit(staff);
  }
}
