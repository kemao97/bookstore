package models.services;

import models.entities.OrderHistory;

import java.util.List;

public interface OrderHistoryService {

  int save(OrderHistory orderHistory);

  List<OrderHistory> findByStaff(int staffId);

  List<OrderHistory> findByOrder(int orderId);

}
