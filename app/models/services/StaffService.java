package models.services;

import forms.StaffSearchForm;
import javafx.util.Pair;
import models.entities.Staff;
import play.data.Form;

import java.util.List;

public interface StaffService {

  Staff retrieve(int staffId);

  Pair<Integer, List<Staff>> search(StaffSearchForm staffSearch, int offset, int limit);

  Staff findByAccount(String accountName);

  int save(Staff staff);

  int edit(Staff staff);

}
