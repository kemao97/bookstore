package models.services;

import models.daos.AccountDAO;
import models.entities.Account;

import javax.inject.Inject;

public class AccountServiceImpl implements AccountService {

  @Inject AccountDAO accountDAO;

  @Override
  public Account retrieve(String accountName) {
    return accountDAO.retrieve(accountName);
  }

  @Override
  public String save(Account account) {
    return accountDAO.save(account);
  }

  @Override
  public void edit(Account account) {
    accountDAO.edit(account);
  }

  @Override
  public Account authenticate(String username, String password) {
    return accountDAO.authenticate(username, password);
  }

  @Override
  public Account facebookAuth(String facebookUid) {
    return accountDAO.facebookAuth(facebookUid);
  }

  @Override
  public Account googleAuth(String googleUid) {
    return accountDAO.googleAuth(googleUid);
  }

}
