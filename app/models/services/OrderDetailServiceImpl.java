package models.services;

import forms.OrderSearchForm;
import javafx.util.Pair;
import models.daos.OrderDetailDAO;
import models.entities.OrderDetail;

import javax.inject.Inject;
import java.util.List;

public class OrderDetailServiceImpl implements OrderDetailService {

  private OrderDetailDAO orderDetailDAO;

  @Inject
  public OrderDetailServiceImpl(OrderDetailDAO orderDetailDAO) {
    this.orderDetailDAO = orderDetailDAO;
  }

  @Override
  public OrderDetail retrieve(int orderDetailId) {
    return orderDetailDAO.retrieve(orderDetailId);
  }

  @Override
  public Pair<Integer, List<OrderDetail>> search(OrderSearchForm orderSearchForm, int offset, int limit) {
    return orderDetailDAO.search(orderSearchForm, offset, limit);
  }

  @Override
  public Pair<Integer, List<OrderDetail>> searchMine(String username, int offset, int limit) {
    return orderDetailDAO.searchMine(username, offset, limit);
  }

  @Override
  public int save(OrderDetail orderDetail) {
    return orderDetailDAO.save(orderDetail);
  }

  @Override
  public void edit(OrderDetail orderDetail) {
    orderDetailDAO.edit(orderDetail);
  }
}
