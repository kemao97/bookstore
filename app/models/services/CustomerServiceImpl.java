package models.services;

import forms.CustomerSearchForm;
import javafx.util.Pair;
import models.entities.Customer;
import models.daos.CustomerDAO;

import javax.inject.Inject;
import java.util.List;

public class CustomerServiceImpl implements CustomerService {

  private CustomerDAO userDAO;

  @Inject
  public CustomerServiceImpl(CustomerDAO userDAO) {
    this.userDAO = userDAO;
  }

  @Override
  public Customer retrieve(int customerId) {
    return userDAO.retrieve(customerId);
  }

  @Override
  public Customer findByAccount(String accountName) {
    return userDAO.findByAccount(accountName);
  }

  @Override
  public Pair<Integer, List<Customer>> search(CustomerSearchForm customerSearch, int offset, int limit) {
    return userDAO.search(customerSearch, offset, limit);
  }

  @Override
  public List<Customer> all() {
    return userDAO.all();
  }

  @Override
  public int save(Customer customer) {
    return userDAO.save(customer);
  }

  @Override
  public int edit(Customer customer) {
    return userDAO.edit(customer);
  }
}
