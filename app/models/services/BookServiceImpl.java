package models.services;

import forms.BookSearchForm;
import javafx.util.Pair;
import models.daos.BookDAO;
import models.entities.Book;

import javax.inject.Inject;
import java.util.List;

public class BookServiceImpl implements BookService {

  private BookDAO bookDAO;

  @Inject
  public BookServiceImpl(BookDAO bookDAO) {
    this.bookDAO = bookDAO;
  }

  @Override
  public Book retrieve(int bookId) {
    return bookDAO.retrieve(bookId);
  }

  @Override
  public List<Book> recentBook(int limit) {
    return bookDAO.recentBook(limit);
  }

  @Override
  public List<Pair<Integer, Book>> bookRank(int limit) {
    return bookDAO.bookRank(limit);
  }

  @Override
  public Pair<Integer, List<Book>> bookSearch(BookSearchForm bookSearch, int offset, int limit) {
    return bookDAO.bookSearch(bookSearch, offset, limit);
  }

  @Override
  public Pair<Integer, List<Book>> search(BookSearchForm bookSearch, int offset, int limit) {
    return bookDAO.search(bookSearch, offset, limit);
  }

  @Override
  public int save(Book book) {
    return bookDAO.save(book);
  }

  @Override
  public void edit(Book book) {
    bookDAO.edit(book);
  }
}
