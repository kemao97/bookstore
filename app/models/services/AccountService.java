package models.services;

import models.entities.Account;

public interface AccountService {

  Account retrieve(String accountName);

  String save(Account account);

  void edit(Account account);

  Account authenticate(String username, String password);

  Account facebookAuth(String facebookUid);

  Account googleAuth(String googleUid);

}
