package models.services;

import forms.OrderSearchForm;
import javafx.util.Pair;
import models.daos.OrderDAO;
import models.entities.Order;

import javax.inject.Inject;
import java.util.List;

public class OrderServiceImpl implements OrderService {

  private OrderDAO orderDAO;

  @Inject
  public OrderServiceImpl(OrderDAO orderDAO) {
    this.orderDAO = orderDAO;
  }

  @Override
  public Order retrieve(int orderId) {
    return orderDAO.retrieve(orderId);
  }

  @Override
  public Pair<Integer, List<Order>> search(OrderSearchForm orderSearchForm, int offset, int limit) {
    return orderDAO.search(orderSearchForm, offset, limit);
  }

  @Override
  public Pair<Integer, List<Order>> searchMine(String username, int offset, int limit) {
    return orderDAO.searchMine(username, offset, limit);
  }

  @Override
  public int save(Order order) {
    return orderDAO.save(order);
  }

  @Override
  public void edit(Order order) {
    orderDAO.edit(order);
  }
}
