package models.services;

import models.daos.OrderHistoryDAO;
import models.entities.OrderHistory;

import javax.inject.Inject;
import java.util.List;

public class OrderHistoryServiceImpl implements OrderHistoryService {

  private OrderHistoryDAO orderHistoryDAO;

  @Inject
  public OrderHistoryServiceImpl(OrderHistoryDAO orderHistoryDAO) {
    this.orderHistoryDAO = orderHistoryDAO;
  }

  @Override
  public int save(OrderHistory orderHistory) {
    return orderHistoryDAO.save(orderHistory);
  }

  @Override
  public List<OrderHistory> findByStaff(int staffId) {
    return orderHistoryDAO.findByStaff(staffId);
  }

  @Override
  public List<OrderHistory> findByOrder(int orderId) {
    return orderHistoryDAO.findByOrder(orderId);
  }
}
