package utils;

import java.text.SimpleDateFormat;
import java.sql.Timestamp;
public class Convert {
  /**
   * format timestamp to string
   */
  public static String formatTimeStamp(Timestamp date) {
    return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(date);
  }

  /**
   * take value last of string
   */
  public static String last(String[] array) {
    String result;
    if(array.length == 0) {
      result = "";
    } else {
      result = array[array.length - 1];
    }
    return result;
  }

}
