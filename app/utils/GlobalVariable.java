package utils;

import java.sql.Timestamp;
import java.util.Random;

public class GlobalVariable {

  /**
   * current timestamp
   */
  public static Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

  /**
   * name get session authentication
   */
  public static String userIdentity = "username";
  public static String userPosition = "position";

  /**
   * authorization
   */
  public static String adminPosition = "admin";
  public static String managerPosition = "manager";
  public static String customerPosition = "customer";
  public static String sellPosition = "seller";
  public static String importPosition = "importer";
  public static String signIn = "signIn";
  public static String notSignIn = "notSignIn";

  public static String getStringRandom() {
    String result = "";
    for(int i = 0; i < 10; i++) {
      result += Math.abs(new Random().nextInt() % 10);
    }
    return result;
  }

}
